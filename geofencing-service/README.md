# KIV/SAR - OpenMatics - Geofencing Service 

## Instalace

Nejprve je třeba nastavit firemní repozitář balíčků - [Nexus](http://www.sonatype.org/nexus/).

### Konfigurace Nexusu

1) Zkopírujte soubor `misc/settings.xml` do adresáře`~/.m2/`
2) Vyplňte jméno a heslo

Případně lze soubor vytvořit z této šablony (je třeba nastavit jméno a heslo):
```
<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd"
                xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <localRepository>${user.home}/.m2/repository</localRepository>

  <servers>
    <server>
      <configuration>
        <timeout>120000</timeout>
      </configuration>
      <id>openmatics-public</id>
       <username>yourLDAP.account</username>
       <password>yourPassword</password>
    </server>
  </servers>

  <mirrors>
    <mirror>
      <url>https://nexus.openmatics.com/repository/maven-public/</url>
      <id>openmatics-public</id>
      <mirrorOf>central</mirrorOf>
   </mirror>
  </mirrors>

  <profiles>
    <profile>
      <id>nexus</id>
      <!--Enable snapshots for the built in central repo to direct -->
      <!--all requests to nexus via the mirror -->
      <repositories>
        <repository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
                  <properties>
            <downloadSources>true</downloadSources>
            <downloadJavadocs>true</downloadJavadocs>
      </properties>
    </profile>
  </profiles>

  <activeProfiles>
    <!--make the profile active all the time -->
    <activeProfile>nexus</activeProfile>
  </activeProfiles>
</settings>
```

Pro více informací:
* [Configuring Apache Maven](https://help.sonatype.com/display/NXRM3/Maven+Repositories#MavenRepositories-ConfiguringApacheMaven)
* [Password Encryption](https://maven.apache.org/guides/mini/guide-encryption.html)


## Generování z archetypu (Zde uvedeno pro informaci)

Generování z archetypu spustíte příkezem:

```bash
$ mvn archetype:generate -DarchetypeGroupId=com.openmatics.cloud.maven -DarchetypeArtifactId=service-archetype -DarchetypeVersion=1.0.2 -DartifactId=kiv-sar-openmatics-geofencing-service -DgroupId=cz.zcu.kiv.sar.openmatics.geofencing -Dversion=1.0.0-SNAPSHOT -Dpackage=cz.zcu.kiv.sar.openmatics.geofencing
```

Interaktivně se vás to zeptá na následující:

```
Define value for property 'scmGroup': geofencing
Define value for property 'useDB': no
Define value for property 'useDeviceMessagingLib': no
Define value for property 'useJSONLib': yes
Define value for property 'useMailSenderLib': no
Define value for property 'useMessaging': yes
Define value for property 'useRESTLib': yes
Define value for property 'useServiceManagementLib': yes
Define value for property 'useServiceMessaging': yes
```

**Poznámka:** Pro připojení do DB je třeba zaškrtnout `Define value for property 'useDB': yes`.

Ručně je třeba změnit verzi internal bomu na `1.0.10`.

## Trip data message (raw)

```javascript
{
  "name": "com.openmatics.app.tripmodel.common.message.TripMessage",
  "version": "1.0.0",
  "deviceid": "7C9763000F2B",
  "assetid": "31a8893a-fa38-44e8-9c06-b46502e74c53",
  "clientid": "a74097be-df0e-43d9-a3b0-0d5242e27298",
  "payload": {
    "tripMessageRecords": [
      {
        "dateTime": "2017-08-31T15:38:58Z",
        "eventType": "DATA",
        "boxIgnitionId": 1503581278559.0,
        "tripDataRecord": {
          "position": {
            "latitude": 49.746829167,
            "longitude": 13.368268833
          },
          "vehicleTotalMileage": 1425654.0,
          "speed": 105.0
        }
      },
      {
        "dateTime": "2017-08-31T15:39:58Z",
        "eventType": "DATA",
        "boxIgnitionId": 1503581278559.0,
        "tripDataRecord": {
          "position": {
            "latitude": 49.746619667,
            "longitude": 13.368269167
          },
          "vehicleTotalMileage": 1425654.0,
          "speed": 105.0
        }
      },
      {
        "dateTime": "2017-08-31T15:40:58Z",
        "eventType": "DATA",
        "boxIgnitionId": 1503581278559.0,
        "tripDataRecord": {
          "position": {
            "latitude": 49.746520333,
            "longitude": 13.368275333
          },
          "vehicleTotalMileage": 1425654.0,
          "speed": 105.0
        }
      },
      {
        "dateTime": "2017-08-31T15:41:58Z",
        "eventType": "DATA",
        "boxIgnitionId": 1503581278559.0,
        "tripDataRecord": {
          "position": {
            "latitude": 49.746421,
            "longitude": 13.368310167
          },
          "vehicleTotalMileage": 1425654.0,
          "speed": 105.0
        }
      },
      {
        "dateTime": "2017-08-31T15:42:58Z",
        "eventType": "DATA",
        "boxIgnitionId": 1503581278559.0,
        "tripDataRecord": {
          "position": {
            "latitude": 49.746493333,
            "longitude": 13.368300333
          },
          "vehicleTotalMileage": 1425654.0,
          "speed": 105.0
        }
      }
    ]
  }
}
```

## Programování

* Pro každé issue vlastní větev s názvem ve formátu `<cislo>_<kratky_nazev>`
* Merge větví `git merge --no-ff <vetev>`


## Tým

* [Petra Volenová](volenova@civ.zcu.cz)
* [Marek Zimmermann](zimmma@civ.zcu.cz)
* [Martin Váňa](vanam@students.zcu.cz)
