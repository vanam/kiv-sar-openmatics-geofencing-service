package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;

@MappedSuperclass
public class BaseObject {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    protected String id;

    @CreationTimestamp
    @Column(updatable = false)
    @DateTimeFormat(pattern = "dd.MM.yyyy H:m")
    protected Instant createDate;

    @UpdateTimestamp
    @DateTimeFormat(pattern = "dd.MM.yyyy H:m")
    protected Instant updateDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy H:m")
    protected Instant deleteDate = null;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public BaseObject() {
    }

    /**
     * @return true if the entity hasn't been persisted yet
     */
    @Transient
    public boolean isNew() {
        return id == null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Instant createDate) {
        this.createDate = createDate;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public Instant getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Instant deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Transient
    public boolean isDeleted() {
        return deleteDate != null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("createDate", createDate)
                .append("updateDate", updateDate)
                .append("deleteDate", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BaseObject that = (BaseObject) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(createDate, that.createDate)
                .append(updateDate, that.updateDate)
                .append(deleteDate, that.deleteDate)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(createDate)
                .append(updateDate)
                .append(deleteDate)
                .toHashCode();
    }
}
