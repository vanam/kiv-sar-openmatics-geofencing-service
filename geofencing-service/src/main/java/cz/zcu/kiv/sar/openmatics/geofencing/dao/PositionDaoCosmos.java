package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.google.gson.Gson;
import com.microsoft.azure.documentdb.*;
import cz.zcu.kiv.sar.openmatics.geofencing.configuration.CosmosProperties;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.PositionCosmos;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * DAO for Cosmos DB.
 * <p>
 * It creates new database and collection if not exist.
 */
@Repository
public class PositionDaoCosmos implements PositionDao {

    /* For database connection */
    private DocumentClient client;

    /* Database name */
    private String database;

    /* Collection name */
    private String collectionID;

    /* Partition key */
    private String partitioningKey;

    /* Predefined request options for communication with database */
    private RequestOptions options;

    /* For creating JSON from objects */
    private Gson gson;

    public PositionDaoCosmos(CosmosProperties cosmosProperties, DocumentClient client) {
        this.client = client;
        this.database = cosmosProperties.getDatabase();
        this.collectionID = cosmosProperties.getCollectionID();
        this.partitioningKey = cosmosProperties.getPartitioningKey().replace("-", "");

        this.options = new RequestOptions();
        this.options.setPartitionKey(new PartitionKey(this.partitioningKey));

        gson = new Gson();
    }

    /**
     * Method saves given position into Cosmos DB under given partition key.
     *
     * @param position contains information about area
     * @throws DocumentClientException error while saving position
     */
    @Override
    public void save(PositionCosmos position) throws DocumentClientException {
        String collectionLink = String.format("/dbs/%s/colls/%s", this.database, this.collectionID);
        position.setClientId(this.partitioningKey);

        String json = getJsonPosition(position);

        client.createDocument(collectionLink, new Document(json), this.options, true);
    }

    /**
     * Method gets list of area id which contains given point.
     *
     * @param point gps location
     * @return list of area id
     */
    @Override
    public List<String> getInsidePositionIds(PositionCosmos point) {
        String json = gson.toJson(point.getLocation()).replace("[[", "[").replace("]]", "]");

        String sql = "SELECT p.id \n" +
                "FROM Positions p \n" +
                "WHERE ST_WITHIN(" + json + ", p.location) \n";

        return getDocuments(sql);
    }

    /**
     * Method gets list of area id which does not contain given point.
     *
     * @param point gps location
     * @return list of area id
     */
    @Override
    public List<String> getOutsidePositionIds(PositionCosmos point) {
        String json = gson.toJson(point.getLocation()).replace("[[", "[").replace("]]", "]");

        String sql = "SELECT p.id \n" +
                "FROM Positions p \n" +
                "WHERE NOT ST_WITHIN(" + json + ", p.location) \n";

        return getDocuments(sql);
    }

    /**
     * Method replaces given position in Cosmos DB according to its id.
     *
     * @param position contains information about area
     * @throws DocumentClientException error while replacing position
     */
    @Override
    public void replace(PositionCosmos position) throws DocumentClientException {
        position.setClientId(this.partitioningKey);
        this.client.replaceDocument(String.format("/dbs/%s/colls/%s/docs/%s", this.database, this.collectionID, position.getId()), new Document(getJsonPosition(position)), this.options);
    }

    /**
     * Method removes given position from Cosmos DB according to its id.
     *
     * @param positionId area id
     * @throws DocumentClientException error while removing position
     */
    @Override
    public void remove(String positionId) throws DocumentClientException {
        this.client.deleteDocument(String.format("/dbs/%s/colls/%s/docs/%s", this.database, this.collectionID, positionId), this.options);
    }

    /**
     * Method gets documents from Cosmos DB according to given SQL.
     *
     * @param sql query
     * @return list of area id
     */
    private List<String> getDocuments(String sql) {
        String collectionLink = String.format("/dbs/%s/colls/%s", this.database, this.collectionID);
        FeedOptions queryOptions = new FeedOptions();
        queryOptions.setPartitionKey(new PartitionKey(this.partitioningKey));

        List<String> data = new ArrayList<>();

        Iterator<Document> it = client.queryDocuments(collectionLink, sql, queryOptions).getQueryIterator();
        while (it.hasNext()) {
            data.add(String.valueOf(it.next().get("id")));
        }

        return data;
    }

    /**
     * Method converts position to JSON.
     *
     * @param positionCosmos contains information about area
     * @return JSON object
     */
    private String getJsonPosition(PositionCosmos positionCosmos) {
        // polygon needs three pairs of [ ]
        return gson.toJson(positionCosmos).replace("[[", "[[[").replace("]]", "]]]") + ")";
    }
}
