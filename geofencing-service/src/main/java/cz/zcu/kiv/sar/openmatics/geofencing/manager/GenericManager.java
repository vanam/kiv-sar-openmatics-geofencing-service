package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;

public interface GenericManager<T extends BaseObject> {

    T findOne(String id);

    Iterable<T> findAll();

    long getCount();

    void remove(String id);

    T save(T item);

}
