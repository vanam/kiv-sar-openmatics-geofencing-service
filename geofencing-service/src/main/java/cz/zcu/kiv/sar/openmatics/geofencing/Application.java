package cz.zcu.kiv.sar.openmatics.geofencing;

import com.openmatics.cloud.lib.messaging.EnableMessaging;
import com.openmatics.cloud.lib.messaging.EnableServiceMessageSender;
import com.openmatics.cloud.lib.rest.EnableREST;
import com.openmatics.cloud.lib.servicemanagement.EnableServiceManagement;
import cz.zcu.kiv.sar.openmatics.geofencing.configuration.ContextConfiguration;
import org.joda.time.DateTimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Locale;
import java.util.TimeZone;

@EnableMessaging
@EnableServiceMessageSender
@EnableServiceManagement
@EnableREST
@ComponentScan("cz.zcu.kiv.sar.openmatics.geofencing")
public class Application {
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        TimeZone.setDefault(TimeZone.getTimeZone(ContextConfiguration.DEFAULT_TIME_ZONE));
        DateTimeZone.setDefault(DateTimeZone.forID(ContextConfiguration.DEFAULT_TIME_ZONE));

        SpringApplication.run(Application.class, args);
    }
}
