package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

class TimeIntervalConverter {
    static final Function<List<TimeInterval>, List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>> modelToDomain = (Function<List<TimeInterval>, List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>>) modelTimeIntervals -> {
        if (modelTimeIntervals == null) {
            return null;
        }

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval> timeIntervals = new ArrayList<>(
                modelTimeIntervals.size());

        for (TimeInterval modelTimeInterval : modelTimeIntervals) {
            timeIntervals.add(new cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval(
                    LocalTime.ofSecondOfDay(modelTimeInterval.getTimeFrom()),
                    LocalTime.ofSecondOfDay(modelTimeInterval.getTimeTo())));
        }

        return timeIntervals;
    };

    static final Function<List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>, List<TimeInterval>> domainToModel = timeIntervals -> {
        if (timeIntervals == null)
            return null;

        List<TimeInterval> modelTimeIntervals = new ArrayList<>(timeIntervals.size());

        for (cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval domainTimeInterval : timeIntervals) {
            modelTimeIntervals.add(new TimeInterval(domainTimeInterval.getTimeFrom().toSecondOfDay(),
                    domainTimeInterval.getTimeTo().toSecondOfDay()));
        }

        return modelTimeIntervals;
    };
}
