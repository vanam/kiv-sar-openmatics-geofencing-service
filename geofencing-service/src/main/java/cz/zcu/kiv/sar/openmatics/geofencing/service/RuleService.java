package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IRuleService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.RuleConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.RuleManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Rule;
import org.springframework.stereotype.Service;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service for serving {@link Rule} objects.
 * <p>
 * Date: 10.11.17
 *
 * @author Marek Zimmermann
 */
@Service
public class RuleService implements IRuleService {
    private AreaManager areaManager;

    private RuleManager ruleManager;

    public RuleService(AreaManager areaManager, RuleManager ruleManager) {
        this.areaManager = areaManager;
        this.ruleManager = ruleManager;
    }

    @Override
    public List<Rule> getRules() {
        Iterable<cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule> domainRules = ruleManager.findAll();

        return StreamSupport
                .stream(domainRules.spliterator(), false)
                .map(RuleConverter::domainToModel)
                .collect(Collectors.toList());
    }

    @Override
    public Rule duplicateRule(String ruleId) {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule domainRule = ruleManager.findOne(ruleId);

        if (domainRule == null) {
            throw new ClientErrorException("Entity 'Rule' not found by ID: '" + ruleId + "'", Response.Status.NOT_FOUND);
        }

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule newRule = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule(domainRule.getName(), domainRule.getDescription(), domainRule.getValidity(), domainRule.getAreas(), domainRule.getVehicles(), domainRule.getFleets(), domainRule.getType());

        return RuleConverter.domainToModel(ruleManager.save(newRule));
    }

    @Override
    public Rule addRule(Rule rule) {
        return RuleConverter.domainToModel(ruleManager.save(RuleConverter.modelToDomain(rule, null, areaManager)));
    }

    @Override
    public Rule updateRule(Rule rule) {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule domainRule = ruleManager.findOne(rule.getId());

        if (domainRule == null) {
            throw new ClientErrorException("Entity 'Rule' not found by ID: '" + rule.getId() + "'", Response.Status.NOT_FOUND);
        }

        return RuleConverter.domainToModel(ruleManager.save(RuleConverter.modelToDomain(rule, domainRule, areaManager)));
    }

    @Override
    public void deleteRule(String id) {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule domainRule = ruleManager.findOne(id);

        if (domainRule == null) {
            throw new ClientErrorException("Entity 'Rule' not found by ID: '" + id + "'", Response.Status.NOT_FOUND);
        }

        ruleManager.remove(id);
    }

}
