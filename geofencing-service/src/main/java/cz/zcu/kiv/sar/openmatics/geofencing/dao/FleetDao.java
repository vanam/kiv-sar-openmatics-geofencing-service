package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;

public interface FleetDao {

    Iterable<Fleet> findAll();

    Iterable<Fleet> findFleetByVehicleId(String vehicleId);
}
