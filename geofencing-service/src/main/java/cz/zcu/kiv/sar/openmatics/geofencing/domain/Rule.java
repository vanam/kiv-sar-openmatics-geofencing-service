package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "`rule`")
public class Rule extends BaseObject {
    @NotEmpty
    private String name;

    @NotNull
    private String description;

    @OneToOne(targetEntity = Validity.class, cascade = CascadeType.ALL)
    private Validity validity;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "rule_area", joinColumns = @JoinColumn(name = "rule_id"), inverseJoinColumns = @JoinColumn(name = "area_id"))
    private List<Area> areas;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    private List<VehicleId> vehicles;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    private List<FleetId> fleets;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public Rule() {
    }

    public Rule(String name, String description, Validity validity, List<Area> areas, List<VehicleId> vehicles,
                List<FleetId> fleets, Type type) {
        update(name, description, validity, areas, vehicles, fleets, type);
    }

    @Transient
    public void update(String name, String description, Validity validity, List<Area> areas, List<VehicleId> vehicles,
                       List<FleetId> fleets, Type type) {
        String validationError = isValid(name, description, validity, areas, vehicles, fleets, type);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        setName(name);
        setDescription(description);
        setValidity(validity);
        setAreas(areas);
        setVehicles(vehicles);
        setFleets(fleets);
        setType(type);
    }

    @Transient
    private String isValid(String name, String description, Validity validity, List<Area> areas,
                           List<VehicleId> vehicles, List<FleetId> fleets, Type type) {
        // Name must not be empty or composed of only whitespace characters
        if (StringUtils.isBlank(name)) {
            return "Mandatory field 'name' cannot be empty.";
        }

        // Description can not be null
        if (description == null) {
            return "Mandatory field 'description' cannot be set to null.";
        }

        // Validity must be an object
        if (validity == null) {
            return "Mandatory field 'validity' cannot be set to null.";
        }

        // There must be at least one valid area for the rule
        if (areas == null || areas.size() == 0) {
            return "Mandatory field 'areas' have to contain at least one area.";
        }

        // There may be no vehicles or fleets, but the lists must be objects
        if (vehicles == null) {
            return "Mandatory field 'vehicles' cannot be set to null.";
        }

        if (fleets == null) {
            return "Mandatory field 'fleets' cannot be set to null.";
        }

        // Rule type must be defined
        if (type == null) {
            return "Mandatory field 'type' cannot be set to null.";
        }

        return null;
    }

    @Transient
    public boolean isApplicableAt(LocalDateTime time) {
        if (validity.getValidationInterval().getDateFrom().compareTo(time.toLocalDate()) * time.toLocalDate()
                .compareTo(validity.getValidationInterval().getDateTo()) < 0) {
            // time is not between valid from and to dates
            return false;
        }

        if (appliesAllTime()) {
            // time is between from and to dates and is applicable always
            return true;
        }

        DayOfWeek dayOfWeek = time.getDayOfWeek();
        List<TimeInterval> intervals;

        switch (dayOfWeek) {
            case MONDAY:
                intervals = validity.getMondayIntervals();
                break;
            case TUESDAY:
                intervals = validity.getTuesdayIntervals();
                break;
            case WEDNESDAY:
                intervals = validity.getWednesdayIntervals();
                break;
            case THURSDAY:
                intervals = validity.getThursdayIntervals();
                break;
            case FRIDAY:
                intervals = validity.getFridayIntervals();
                break;
            case SATURDAY:
                intervals = validity.getSaturdayIntervals();
                break;
            case SUNDAY:
                intervals = validity.getSundayIntervals();
                break;
            default:
                intervals = new ArrayList<>();
                break;
        }

        if (intervals.isEmpty()) {
            // rule is not always applicable (as checked higher), but we have no time intervals for current day -
            // - that's a no
            return false;
        }

        for (TimeInterval interval : intervals) {
            if (interval.getTimeFrom().compareTo(time.toLocalTime()) * time.toLocalTime().compareTo(interval.getTimeTo()) >= 0) {
                // we are inside the time interval
                return true;
            }
        }

        // we found the right time intervals, but we are not inside any of them
        return false;
    }

    /**
     * Checks whether the rule is applicable independently on the time intervals (at all times) or not. That happens
     * when there are no time intervals at all.
     *
     * @return true if rule should be applicable always between the 2 validity dates
     */
    public boolean appliesAllTime() {
        int tiCount = 0;
        tiCount += validity.getMondayIntervals().size();
        tiCount += validity.getTuesdayIntervals().size();
        tiCount += validity.getWednesdayIntervals().size();
        tiCount += validity.getThursdayIntervals().size();
        tiCount += validity.getFridayIntervals().size();
        tiCount += validity.getSaturdayIntervals().size();
        tiCount += validity.getSundayIntervals().size();

        if (tiCount == 0) {
            // no time intervals - rule always apply
            return true;
        }

        // there are some time intervals - rule does not always apply
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Validity getValidity() {
        return validity;
    }

    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<VehicleId> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleId> vehicles) {
        this.vehicles = vehicles;
    }

    public List<FleetId> getFleets() {
        return fleets;
    }

    public void setFleets(List<FleetId> fleets) {
        this.fleets = fleets;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("type", type)
                .append("createInstant", createDate)
                .append("updateInstant", updateDate)
                .append("deleteInstant", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(name, rule.name)
                .append(description, rule.description)
                .append(validity, rule.validity)
                .append(areas, rule.areas)
                .append(vehicles, rule.vehicles)
                .append(fleets, rule.fleets)
                .append(type, rule.type)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(name)
                .append(description)
                .append(validity)
                .append(areas)
                .append(vehicles)
                .append(fleets)
                .append(type)
                .toHashCode();
    }

    public enum Type {
        INSIDE, OUTSIDE
    }
}
