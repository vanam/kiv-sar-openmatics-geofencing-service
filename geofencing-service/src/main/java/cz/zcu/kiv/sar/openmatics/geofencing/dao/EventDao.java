package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;

public interface EventDao extends GenericDao<Event> {

}
