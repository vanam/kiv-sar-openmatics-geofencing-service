package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import org.springframework.stereotype.Repository;

@Repository
public class AreaDaoJpa extends GenericDaoJpa<Area> implements AreaDao {

    public AreaDaoJpa() {
        super(Area.class);
    }

}
