package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Entity containing GPS coordinates for area definition.
 * <p>
 * Date: 20.11.17
 *
 * @author Marek Zimmermann
 */
@Entity
public class Position extends BaseObject {
    private double latitude;

    private double longitude;

    @Column(name = "`order`")
    private int order;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public Position() {
    }

    public Position(double latitude, double longitude, int order) {
        String validationError = isValid(latitude, longitude, order);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        this.latitude = latitude;
        this.longitude = longitude;
        this.order = order;
    }

    @Transient
    private String isValid(double latitude, double longitude, int order) {
        //https://msdn.microsoft.com/en-us/library/aa578799.aspx
        //latitude must be in range of -90 to 90
        if (latitude < -90 || latitude > 90) {
            return "Mandatory field 'latitude' must be in range of -90 to 90.";
        }

        //longitude must be in range of -180 to 180
        if (longitude < -180 || longitude > 180) {
            return "Mandatory field 'longitude' must be in range of -180 to 180.";
        }

        return null;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("latitude", latitude)
                .append("longitude", longitude)
                .append("order", order)
                .append("createDate", createDate)
                .append("updateDate", updateDate)
                .append("deleteDate", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(latitude, position.latitude)
                .append(longitude, position.longitude)
                .append(order, position.order)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(latitude)
                .append(longitude)
                .append(order)
                .toHashCode();
    }
}
