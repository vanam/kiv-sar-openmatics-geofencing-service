package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;

public interface FleetManager {

    Iterable<Fleet> findAll();

    Iterable<Fleet> findFleetByVehicleId(String vehicleId);

}
