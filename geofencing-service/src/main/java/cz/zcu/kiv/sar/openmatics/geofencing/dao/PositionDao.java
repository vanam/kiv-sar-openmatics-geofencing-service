package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.microsoft.azure.documentdb.DocumentClientException;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.PositionCosmos;

import java.util.List;

public interface PositionDao {

    void save(PositionCosmos position) throws DocumentClientException;

    List<String> getInsidePositionIds(PositionCosmos point);

    List<String> getOutsidePositionIds(PositionCosmos point);

    void replace(PositionCosmos position) throws DocumentClientException;

    void remove(String positionId) throws DocumentClientException;
}
