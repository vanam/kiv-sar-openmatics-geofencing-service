package cz.zcu.kiv.sar.openmatics.geofencing.dao;


import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.Instant;

/**
 * JPA implementation of the {@link GenericDao} interface.
 */
abstract public class GenericDaoJpa<T extends BaseObject> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    private Class<T> persistedType;

    /**
     * @param persistedType type of the entity persisted by this DAO
     */
    public GenericDaoJpa(Class<T> persistedType) {
        this.persistedType = persistedType;
    }

    @Override
    public T save(T item) {
        if (item.isNew()) {
            em.persist(item);
            return item;
        } else {
            return em.merge(item);
        }
    }

    @Override
    public T findOne(String id) {
        TypedQuery<T> q = em.createQuery("SELECT p FROM " + persistedType.getCanonicalName() + " p WHERE p.deleteDate IS NULL AND p.id = :id", persistedType);
        q.setParameter("id", id);

        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            Logger.getLogger(GenericDaoJpa.class.getName()).log(Level.ERROR, null, e);
            return null;
        }
    }

    @Override
    public Iterable<T> findAll() {
        TypedQuery<T> q = em.createQuery("SELECT p FROM " + persistedType.getCanonicalName() + " p WHERE p.deleteDate IS NULL", persistedType);

        try {
            return q.getResultList();
        } catch (NoResultException e) {
            Logger.getLogger(GenericDaoJpa.class.getName()).log(Level.ERROR, null, e);
            return null;
        }
    }

    @Override
    public long getCount() {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);

        Root<T> root = cq.from(persistedType);
        cq.select(qb.count(root));
        cq.where(qb.isNull(root.get("deleteDate")));

        try {
            return em.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            Logger.getLogger(GenericDaoJpa.class.getName()).log(Level.ERROR, null, e);
            return 0;
        }
    }

    @Override
    public void remove(T item) {
        if (!item.isNew()) {
            // Soft delete
            item.setDeleteDate(Instant.now());
            this.save(item);
        }
    }
}
