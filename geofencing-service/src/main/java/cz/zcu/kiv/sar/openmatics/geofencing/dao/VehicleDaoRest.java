package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.openmatics.cloud.api.asset.management.IVehicleService;
import com.openmatics.cloud.lib.servicemanagement.util.IServiceInformationHolder;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleDaoRest implements VehicleDao {

    private IVehicleService vehicleService;

    private IServiceInformationHolder serviceInformationHolder;

    public VehicleDaoRest(IVehicleService vehicleService, IServiceInformationHolder serviceInformationHolder) {
        this.vehicleService = vehicleService;
        this.serviceInformationHolder = serviceInformationHolder;
    }

    @Override
    public Iterable<Vehicle> findAll() {
        List<com.openmatics.cloud.core.api.Vehicle> vehiclesByClientId = vehicleService.getVehiclesByClientId(serviceInformationHolder.getServiceClientId());
        List<Vehicle> vehicleList = new ArrayList<>(vehiclesByClientId.size());

        for (com.openmatics.cloud.core.api.Vehicle vehicle : vehiclesByClientId) {
            Vehicle newVehicle = new Vehicle(vehicle.getId(), vehicle.getName());
            vehicleList.add(newVehicle);
        }

        return vehicleList;
    }
}
