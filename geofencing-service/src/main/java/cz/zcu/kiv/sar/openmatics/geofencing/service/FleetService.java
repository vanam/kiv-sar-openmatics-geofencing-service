package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IFleetService;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.FleetManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for serving {@link Fleet} objects.
 * <p>
 * Date: 10.11.17
 *
 * @author Marek Zimmermann
 */
@Service
public class FleetService implements IFleetService {
    private FleetManager fleetManager;

    public FleetService(FleetManager fleetManager) {
        this.fleetManager = fleetManager;
    }

    @Override
    public List<Fleet> getFleets() {
        return (List<Fleet>) fleetManager.findAll();
    }
}
