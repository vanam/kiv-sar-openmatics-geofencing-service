package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.VehicleDao;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.springframework.stereotype.Service;

@Service
public class VehicleManagerImpl implements VehicleManager {

    private VehicleDao vehicleDao;

    public VehicleManagerImpl(VehicleDao vehicleDao) {
        this.vehicleDao = vehicleDao;
    }

    @Override
    public Iterable<Vehicle> findAll() {
        return vehicleDao.findAll();
    }
}
