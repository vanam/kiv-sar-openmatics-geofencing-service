package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.FleetId;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.VehicleId;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.*;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class RuleConverter {
    public static final cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule modelToDomain(Rule modelRule,
                                                                                       cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule domainRule,
                                                                                       AreaManager areaManager) {
        if (modelRule == null) {
            return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule(null, null, null, null, null, null, null);
        }

        Validity modelRuleValidity = modelRule.getValidity();
        List<Area> modelRuleAreaList = modelRule.getAreaList();
        List<String> modelRuleVehicleList = modelRule.getVehicleIdList();
        List<String> modelRuleFleetList = modelRule.getFleetIdList();
        Rule.Type modelRuleType = modelRule.getType();

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity validity = ValidityConverter.modelToDomain
                .apply(modelRuleValidity);

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.Area> areas;
        if (modelRuleAreaList == null) {
            areas = null;
        } else {
            areas = new ArrayList<>(modelRuleAreaList.size());
            for (Area area : modelRuleAreaList) {
                cz.zcu.kiv.sar.openmatics.geofencing.domain.Area foundArea = areaManager.findOne(area.getId());
                if (foundArea == null) {
                    //area with given ID not found
                    throw new ClientErrorException("Entity 'Area' not found by ID: '" + area.getId() + "'",
                            Response.Status.NOT_FOUND);
                }

                areas.add(foundArea);
            }
        }

        List<VehicleId> vehicleIdList;
        if (modelRuleVehicleList == null) {
            vehicleIdList = null;
        } else {
            vehicleIdList = new ArrayList<>(modelRuleVehicleList.size());
            for (String id : modelRuleVehicleList) {
                vehicleIdList.add(new VehicleId(id));
            }
        }

        List<FleetId> fleetIdList;
        if (modelRuleFleetList == null) {
            fleetIdList = null;
        } else {
            fleetIdList = new ArrayList<>(modelRuleFleetList.size());
            for (String id : modelRuleFleetList) {
                fleetIdList.add(new FleetId(id));
            }
        }

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule.Type type;
        if (modelRuleType == null) {
            type = null;
        } else {
            switch (modelRuleType) {
                default:
                case INSIDE:
                    type = cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule.Type.INSIDE;
                    break;
                case OUTSIDE:
                    type = cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule.Type.OUTSIDE;
                    break;
            }
        }

        if (domainRule != null) {
            domainRule.update(modelRule.getName(), modelRule.getDescription(), validity, areas, vehicleIdList,
                    fleetIdList, type);
            return domainRule;
        } else {
            return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule(modelRule.getName(), modelRule.getDescription(),
                    validity, areas, vehicleIdList, fleetIdList, type);
        }
    }

    public static final Rule domainToModel(cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule domainRule) {
        if (domainRule == null)
            return null;

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity domainRuleValidity = domainRule.getValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.Area> domainRuleAreas = domainRule.getAreas();
        List<VehicleId> domainRuleVehicles = domainRule.getVehicles();
        List<FleetId> domainRuleFleets = domainRule.getFleets();
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule.Type domainRuleType = domainRule.getType();

        Validity validity = ValidityConverter.domainToModel.apply(domainRuleValidity);

        List<Area> ruleAreas;
        if (domainRuleAreas == null) {
            ruleAreas = null;
        } else {
            ruleAreas = new ArrayList<>(domainRuleAreas.size());
            for (cz.zcu.kiv.sar.openmatics.geofencing.domain.Area domainRuleArea : domainRuleAreas) {
                ruleAreas.add(AreaConverter.domainToModel.apply(domainRuleArea));
            }
        }

        List<String> ruleVehicles;
        if (domainRuleVehicles == null) {
            ruleVehicles = null;
        } else {
            ruleVehicles = new ArrayList<>(domainRuleVehicles.size());
            for (VehicleId domainRuleVehicle : domainRuleVehicles) {
                ruleVehicles.add(String.valueOf(domainRuleVehicle.getVehicleId()));
            }
        }

        List<String> ruleFleets;
        if (domainRuleFleets == null) {
            ruleFleets = null;
        } else {
            ruleFleets = new ArrayList<>(domainRuleFleets.size());
            for (FleetId domainRuleFleet : domainRuleFleets) {
                ruleFleets.add(String.valueOf(domainRuleFleet.getFleetId()));
            }
        }

        Rule.Type type;
        if (domainRuleType == null) {
            type = null;
        } else {
            switch (domainRuleType) {
                default:
                case INSIDE:
                    type = Rule.Type.INSIDE;
                    break;
                case OUTSIDE:
                    type = Rule.Type.OUTSIDE;
                    break;
            }
        }

        return new Rule(domainRule.getId(), domainRule.getName(), domainRule.getDescription(), validity, ruleAreas,
                ruleVehicles, ruleFleets, type);
    }
}
