package cz.zcu.kiv.sar.openmatics.geofencing.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.documentdb.*;
import com.openmatics.app.tripmodel.common.message.TripMessage;
import com.openmatics.cloud.api.asset.management.IVehicleService;
import com.openmatics.cloud.api.asset.management.properties.AssetManagementServiceProperties;
import com.openmatics.cloud.core.servicemanagement.model.destination.MessageDestination;
import com.openmatics.cloud.fleetmanagement.api.IFleetService;
import com.openmatics.cloud.fleetmanagement.properties.FleetManagementServiceProperties;
import com.openmatics.cloud.lib.rest.contracts.JAXRSContract;
import com.openmatics.cloud.lib.rest.decoder.ContentTypeSensitiveJacksonDecoder;
import com.openmatics.cloud.lib.rest.decoder.RestErrorObjectReflectionBasedDecoder;
import com.openmatics.cloud.lib.rest.oauth.AuthenticatedTarget;
import com.openmatics.cloud.lib.rest.oauth.ITokenProvider;
import com.openmatics.cloud.lib.servicemanagement.communication.common.exception.DestinationRegistrationException;
import com.openmatics.cloud.lib.servicemanagement.communication.message.receiving.IMessageListenerService;
import com.openmatics.cloud.lib.servicemanagement.communication.message.receiving.MessageListener;
import com.openmatics.cloud.lib.servicemanagement.configuration.properties.ServiceManagementProperties;
import com.openmatics.cloud.lib.servicemanagement.destinations.model.RegisteredDestinations;
import com.openmatics.cloud.lib.servicemanagement.destinations.util.IDestinationFactory;
import com.openmatics.cloud.lib.servicemanagement.util.IServiceInformationHolder;
import cz.zcu.kiv.sar.openmatics.geofencing.listener.TripMessageListener;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.EventManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.RuleManager;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import feign.Feign;
import feign.codec.Decoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Collections;

/**
 * Main application spring configuration.
 * <p>
 * Date: 9.11.17
 *
 * @author Marek Zimmermann
 */
@EnableAsync
@Configuration
@EnableConfigurationProperties({
        AssetManagementServiceProperties.class,
        FleetManagementServiceProperties.class,
        ServiceManagementProperties.class,
        MessageProperties.class,
        CosmosProperties.class})
public class ContextConfiguration {
    /**
     * Defined time zone to be used everywhere where {@link java.util.TimeZone} or
     * {@link org.joda.time.DateTimeZone} instance needs to be used.
     */
    public static final String DEFAULT_TIME_ZONE = "Etc/GMT+0";

    /**
     * Defined time zone offset to be used everywhere where {@link java.time.ZoneOffset} instance needs to be used.
     */
    public static final String DEFAULT_TIME_ZONE_OFFSET = "Z";

    // Flag for turning off consuming messages from a queue
    private @Value("${message.on:false}")
    boolean messageOn;

    @Autowired
    private AssetManagementServiceProperties assetManagementServiceConfig;

    @Autowired
    private FleetManagementServiceProperties fleetManagementServiceConfig;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ITokenProvider tokenProvider;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private IDestinationFactory destinationFactory;

    @Autowired
    private CosmosProperties cosmosProperties;

    @Autowired
    private IMessageListenerService messageListenerService;

    @Autowired
    RuleManager ruleManager;

    @Autowired
    EventManager eventManager;

    @Autowired
    IServiceInformationHolder serviceInformationHolder;
    
    /* --- Bean definitions --- */

    @Bean
    public IVehicleService vehicleManagementService() {
        return buildFeignClientWithReflectionErrorDecoder(IVehicleService.class, assetManagementServiceConfig.getUrl(), assetManagementServiceConfig.getClientId());
    }

    @Bean
    public IFleetService fleetManagementService() {
        return buildFeignClientWithReflectionErrorDecoder(IFleetService.class, fleetManagementServiceConfig.getUrl(), fleetManagementServiceConfig.getClientId());
    }

    @Bean
    public Decoder getFeignDecoder() {
        return new ContentTypeSensitiveJacksonDecoder(objectMapper);
    }

    @Bean
    public RegisteredDestinations getRegisteredDestinations() {
        if (!messageOn) {
            return new RegisteredDestinations(null, null, null);
        }

        MessageDestination messageDestination = getTripMessageDestination();
        return new RegisteredDestinations(Collections.singletonList(messageDestination), null, null);
    }

    private MessageDestination getTripMessageDestination() {
        return destinationFactory.createMessageDestination(
                messageProperties.getQueue(), messageProperties.getType(),
                messageProperties.getVersion());
    }

    @Bean
    public MessageListener<TripMessage> messageProcessor() throws DestinationRegistrationException {
        if ( ! messageOn) {
            return null;
        }

        MessageListener<TripMessage> messageListener = new TripMessageListener(ruleManager, eventManager, serviceInformationHolder);
        messageListenerService.registerMessageListener(messageListener, getTripMessageDestination(), TripMessage.class);

        return messageListener;
    }

    @SuppressFBWarnings(value = "DM_EXIT")
    @Bean @Async
    public DocumentClient getDocumentClient () {
        DocumentClient client = new DocumentClient(cosmosProperties.getUri(), cosmosProperties.getKey(), ConnectionPolicy.GetDefault(), ConsistencyLevel.Session);

        try {
            createDatabase(client);
            createCollection(client);
        } catch (DocumentClientException e) {
            System.exit(1);
        }

        return client;
    }

    /**
     * If database with given name does not exist in Cosmos DB, it is created.
     *
     * @throws DocumentClientException error while creating new database
     */
    private void createDatabase(DocumentClient client) throws DocumentClientException {
        String databaseLink = String.format("/dbs/%s", cosmosProperties.getDatabase());

        // Check to verify a database does not exist
        try {
            client.readDatabase(databaseLink, null);
        } catch (DocumentClientException de) {
            // If the database does not exist, create a new database
            if (de.getStatusCode() == 404) {
                Database databaseDefinition = new Database();
                databaseDefinition.setId(cosmosProperties.getDatabase());
                client.createDatabase(databaseDefinition, null);
            } else {
                throw de;
            }
        }
    }

    /**
     * If collection with given name does not exist in Cosmos DB, it is created.
     *
     * @throws DocumentClientException error while creating new collection
     */
    private void createCollection(DocumentClient client) throws DocumentClientException {
        String databaseLink = String.format("/dbs/%s", cosmosProperties.getDatabase());
        String collectionLink = String.format("/dbs/%s/colls/%s", cosmosProperties.getDatabase(), cosmosProperties.getCollectionID());

        try {
            client.readCollection(collectionLink, null);
        } catch (DocumentClientException de) {
            // If the document collection does not exist, create a new collection
            if (de.getStatusCode() == 404) {
                DocumentCollection collectionInfo = new DocumentCollection();
                collectionInfo.setId(cosmosProperties.getCollectionID());
                PartitionKeyDefinition partitionKeyDefinition = new PartitionKeyDefinition();
                partitionKeyDefinition.setPaths(Collections.singletonList("/clientId"));
                collectionInfo.setPartitionKey(partitionKeyDefinition);

                client.createCollection(databaseLink, collectionInfo, null);
            } else {
                throw de;
            }
        }
    }

    /* --- Helper methods --- */
    private <T> T buildFeignClientWithReflectionErrorDecoder(Class<T> serviceClass, String serviceUrl, String appClientId) {

        return Feign.builder().contract(new JAXRSContract()) //use JAXRS Contract
                .errorDecoder(new RestErrorObjectReflectionBasedDecoder(objectMapper)).decoder(getFeignDecoder()).encoder(new JacksonEncoder(objectMapper))
                .target(new AuthenticatedTarget<>(serviceClass, serviceUrl, appClientId, tokenProvider));
    }
}
