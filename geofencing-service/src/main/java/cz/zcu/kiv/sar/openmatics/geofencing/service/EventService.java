package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IEventService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.EventConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.EventManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Event;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service for serving {@link Event} objects.
 * <p>
 * Date: 10.11.17
 *
 * @author Marek Zimmermann
 */
@Service
public class EventService implements IEventService {

    private EventManager eventManager;

    public EventService(EventManager eventManager) {
        this.eventManager = eventManager;
    }

    @Override
    public List<Event> getEvents() {
        Iterable<cz.zcu.kiv.sar.openmatics.geofencing.domain.Event> domainEvents = eventManager.findAll();

        return StreamSupport
                .stream(domainEvents.spliterator(), false)
                .map(EventConverter.domainToModel)
                .collect(Collectors.toList());
    }
}
