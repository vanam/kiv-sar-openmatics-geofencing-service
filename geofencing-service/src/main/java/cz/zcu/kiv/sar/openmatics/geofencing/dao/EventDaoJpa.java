package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;
import org.springframework.stereotype.Repository;

@Repository
public class EventDaoJpa extends GenericDaoJpa<Event> implements EventDao {

    public EventDaoJpa() {
        super(Event.class);
    }

}