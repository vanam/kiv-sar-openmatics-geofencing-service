package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.time.LocalDate;

/**
 * Entity containing information for defining date range for rule evaluation.
 * <p>
 * Date: 19.11.17
 *
 * @author Marek Zimmermann
 */
@Embeddable
public class ValidationInterval {
    @NotEmpty
    private LocalDate dateFrom;

    @NotEmpty
    private LocalDate dateTo;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public ValidationInterval() {
    }

    public ValidationInterval(LocalDate dateFrom, LocalDate dateTo) {
        String validationError = isValid(dateFrom, dateTo);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    @Transient
    private String isValid(LocalDate dateFrom, LocalDate dateTo) {
        if (dateFrom == null) {
            return "Mandatory field 'dateFrom' cannot be set to null.";
        }

        if (dateTo == null) {
            return "Mandatory field 'dateTo' cannot be set to null.";
        }

        return null;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("dateFrom", dateFrom)
                .append("dateTo", dateTo)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ValidationInterval interval = (ValidationInterval) o;

        return new EqualsBuilder()
                .append(dateFrom, interval.dateFrom)
                .append(dateTo, interval.dateTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(dateFrom)
                .append(dateTo)
                .toHashCode();
    }
}
