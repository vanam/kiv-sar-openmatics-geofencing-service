package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule;

import java.util.List;

public interface RuleDao extends GenericDao<Rule> {

    List<Rule> findRulesByVehicleId(String vehicleId);

    List<Rule> findRulesByFleetId(String fleetId);

}
