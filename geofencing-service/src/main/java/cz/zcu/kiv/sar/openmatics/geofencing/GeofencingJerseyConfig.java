package cz.zcu.kiv.sar.openmatics.geofencing;

import com.openmatics.cloud.lib.rest.jersey.JerseyConfig;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class GeofencingJerseyConfig extends JerseyConfig {

    public GeofencingJerseyConfig() {
        register(ExceptionMapper.class);
    }
}
