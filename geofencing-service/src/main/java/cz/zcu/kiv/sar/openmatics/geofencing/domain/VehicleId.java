package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Entity containing vehicle ID.
 * <p>
 * Date: 20.11.17
 *
 * @author Marek Zimmermann
 */
@Entity
public class VehicleId {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    private String vehicleId;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public VehicleId() {
    }

    public VehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    /**
     * @return true if the entity hasn't been persisted yet
     */
    @Transient
    public boolean isNew() {
        return id == null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("vehicleId", vehicleId)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VehicleId vehicleId1 = (VehicleId) o;

        return new EqualsBuilder()
                .append(id, vehicleId1.id)
                .append(vehicleId, vehicleId1.vehicleId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(vehicleId)
                .toHashCode();
    }
}
