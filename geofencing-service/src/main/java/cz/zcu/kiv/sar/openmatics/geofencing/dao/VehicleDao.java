package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;

public interface VehicleDao {

    Iterable<Vehicle> findAll();
}
