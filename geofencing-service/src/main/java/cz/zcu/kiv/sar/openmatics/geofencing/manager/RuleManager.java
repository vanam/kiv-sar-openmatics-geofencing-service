package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.RuleViolation;

import java.time.LocalDateTime;
import java.util.List;

public interface RuleManager extends GenericManager<Rule> {

    List<RuleViolation> matchRules(String vehicleId, LocalDateTime time, double latitude, double longitude);

}
