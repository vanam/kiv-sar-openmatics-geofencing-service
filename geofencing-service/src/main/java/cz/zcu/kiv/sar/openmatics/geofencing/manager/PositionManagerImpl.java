package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import com.microsoft.azure.documentdb.DocumentClientException;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.CosmosConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.PositionDao;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.PositionCosmos;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionManagerImpl implements PositionManager {

    private PositionDao positionDao;

    public PositionManagerImpl(PositionDao positionDao) {
        this.positionDao = positionDao;
    }

    @Override
    public void save(Area area) throws DocumentClientException {
        positionDao.save(convertArea(area));
    }

    @Override
    public List<String> getInsidePositionIds(double longitude, double latitude) {
        return positionDao.getInsidePositionIds(CosmosConverter.convertPointToPositionCosmos(longitude, latitude));
    }

    @Override
    public List<String> getOutsidePositionIds(double longitude, double latitude) {
        return positionDao.getOutsidePositionIds(CosmosConverter.convertPointToPositionCosmos(longitude, latitude));
    }

    @Override
    public void replace(Area area) throws DocumentClientException {
        positionDao.replace(convertArea(area));
    }

    @Override
    public void remove(String areaId) throws DocumentClientException {
        positionDao.remove(areaId);
    }

    private PositionCosmos convertArea(Area area) {
        PositionCosmos positionCosmos;

        switch (area.getType()) {
            default:
            case POLYGON:
                positionCosmos = CosmosConverter.convertPolygonAreaToPositionCosmos(area);
                break;
            case RECTANGLE:
                positionCosmos = CosmosConverter.convertRectangleAreaToPositionCosmos(area);
                break;
            case CIRCLE:
                positionCosmos = CosmosConverter.convertCircleAreaToPositionCosmos(area);
                break;
        }
        return positionCosmos;
    }
}
