package cz.zcu.kiv.sar.openmatics.geofencing.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Message properties configuration
 */
@ConfigurationProperties(prefix = "message")
public class MessageProperties {

    private String queue;

    private String type;

    private String version;

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}