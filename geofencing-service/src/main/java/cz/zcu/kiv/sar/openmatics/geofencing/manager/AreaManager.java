package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;

public interface AreaManager extends GenericManager<Area> {

}
