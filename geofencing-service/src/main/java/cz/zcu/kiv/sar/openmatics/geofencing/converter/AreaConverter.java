package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Position;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class AreaConverter {
    /**
     * Converts the model Area to domain Area.
     *
     * @return converted instance
     */
    public static final Function<Area, cz.zcu.kiv.sar.openmatics.geofencing.domain.Area> modelToDomain = area -> {
        if (area == null) {
            return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(null, null, null);
        }

        Area.AreaType modelAreaType = area.getType();
        List<Position> modelAreaPositionList = area.getPositionList();

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType areaType;

        if (modelAreaType == null) {
            areaType = null;
        } else {
            switch (modelAreaType) {
                default:
                case RECTANGLE:
                    areaType = cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE;
                    break;
                case CIRCLE:
                    areaType = cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.CIRCLE;
                    break;
                case POLYGON:
                    areaType = cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.POLYGON;
                    break;
            }
        }

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.Position> areaPositions;
        if (modelAreaPositionList == null) {
            areaPositions = null;
        } else {
            int order = 0;
            areaPositions = new ArrayList<>(
                    modelAreaPositionList.size());
            for (Position modelAreaPosition : modelAreaPositionList) {
                areaPositions
                        .add(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(modelAreaPosition.getLatitude(),
                                modelAreaPosition.getLongitude(), order++));
            }
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(area.getName(), areaType, areaPositions);
    };

    /**
     * Converts the domain Area to model Area.
     *
     * @return converted instance
     */
    public static final Function<cz.zcu.kiv.sar.openmatics.geofencing.domain.Area, Area> domainToModel = area -> {
        if (area == null) {
            return null;
        }

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType domainAreaType = area.getType();
        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.Position> domainAreaPositions = area.getPositions();

        Area.AreaType areaType;

        if (domainAreaType == null) {
            areaType = null;
        } else {
            switch (domainAreaType) {
                default:
                case RECTANGLE:
                    areaType = Area.AreaType.RECTANGLE;
                    break;
                case CIRCLE:
                    areaType = Area.AreaType.CIRCLE;
                    break;
                case POLYGON:
                    areaType = Area.AreaType.POLYGON;
                    break;
            }
        }

        List<Position> areaPositions;
        if (domainAreaPositions == null) {
            areaPositions = null;
        } else {
            areaPositions = new ArrayList<>(domainAreaPositions.size());
            for (cz.zcu.kiv.sar.openmatics.geofencing.domain.Position domainAreaPosition : domainAreaPositions) {
                areaPositions.add(new Position(domainAreaPosition.getLatitude(), domainAreaPosition.getLongitude()));
            }
        }

        return new Area(area.getId(), area.getName(), areaType, areaPositions,
                capped(area.getCreateDate()).toEpochMilli());
    };

    /**
     * Method used for resolving the overflow/underflow issue of getting the time from epoch in milliseconds to long
     * without using exception throwing, which is better performance wise.
     * <p>
     * Method puts 3 {@link Instant} instances into an array: ours, the one with acceptable min value for long and the
     * one with acceptable max value for long. Then it sorts them and returns the middle one which is always the one
     * that does not overflow or underflow.
     *
     * @param instant our instant instance, from which we want to get epoch time as long
     * @return instant with always extractable toEpochMilli into long
     */
    public static Instant capped(Instant instant) {
        Instant[] instants = {Instant.ofEpochMilli(Long.MIN_VALUE), instant, Instant.ofEpochMilli(Long.MAX_VALUE)};
        Arrays.sort(instants);
        return instants[1];
    }
}