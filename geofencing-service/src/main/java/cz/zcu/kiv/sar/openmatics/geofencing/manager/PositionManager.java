package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import com.microsoft.azure.documentdb.DocumentClientException;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;

import java.util.List;

public interface PositionManager {

    void save(Area area) throws DocumentClientException;

    List<String> getInsidePositionIds(double longitude, double latitude);

    List<String> getOutsidePositionIds(double longitude, double latitude);

    void replace(Area area) throws DocumentClientException;

    void remove(String areaId) throws DocumentClientException;
}
