package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval;
import cz.zcu.kiv.sar.openmatics.geofencing.model.ValidationInterval;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Validity;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

public class ValidityConverter
{
    public static final Function<Validity, cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity> modelToDomain = validity -> {
        if (validity == null)
        {
            return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity(null, null, null, null, null, null, null,
                    null);
        }

        ValidationInterval validationInterval = validity.getValidationInterval();
        List<TimeInterval>[] timeIntervals = validity.getTimeIntervals();

        cz.zcu.kiv.sar.openmatics.geofencing.domain.ValidationInterval domainValidationInterval;
        if (validationInterval == null)
        {
            domainValidationInterval = null;
        }
        else
        {
            domainValidationInterval = new cz.zcu.kiv.sar.openmatics.geofencing.domain.ValidationInterval(
                    LocalDate.ofEpochDay(validationInterval.getDateFrom()),
                    LocalDate.ofEpochDay(validationInterval.getDateTo()));
        }

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>[] domainTimeIntervals = new List[7];

        //check if model has time intervals and have exactly 7 lists of them - if not, contract violation - refuse it
        if (timeIntervals == null || timeIntervals.length != 7)
        {
            //not a valid model object for transformation to domain object
            for (int i = 0; i < domainTimeIntervals.length; i++)
            {
                domainTimeIntervals[i] = null;
            }
        }
        else
        {
            for (int i = 0; i < 7; i++)
            {
                domainTimeIntervals[i] = TimeIntervalConverter.modelToDomain.apply(timeIntervals[i]);
            }
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity(domainValidationInterval,
                domainTimeIntervals[0], domainTimeIntervals[1], domainTimeIntervals[2], domainTimeIntervals[3],
                domainTimeIntervals[4], domainTimeIntervals[5], domainTimeIntervals[6]);
    };

    public static final Function<cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity, Validity> domainToModel = validity -> {
        if (validity == null)
        {
            return null;
        }

        cz.zcu.kiv.sar.openmatics.geofencing.domain.ValidationInterval validationInterval = validity
                .getValidationInterval();


        ValidationInterval modelValidationInterval;
        if (validationInterval == null || validationInterval.getDateFrom() == null || validationInterval
                .getDateTo() == null)
        {
            modelValidationInterval = null;
        }
        else
        {
            modelValidationInterval = new ValidationInterval(validationInterval.getDateFrom().toEpochDay(),
                    validationInterval.getDateTo().toEpochDay());
        }

        List<TimeInterval>[] modelTimeIntervals = new List[7];
        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>[] domainTimeIntervals = new List[]{
                validity.getMondayIntervals(),
                validity.getTuesdayIntervals(),
                validity.getWednesdayIntervals(),
                validity.getThursdayIntervals(),
                validity.getFridayIntervals(),
                validity.getSaturdayIntervals(),
                validity.getSundayIntervals()
        };

        for (int i = 0; i < 7; i++)
        {
            modelTimeIntervals[i] = TimeIntervalConverter.domainToModel.apply(domainTimeIntervals[i]);
        }

        return new Validity(modelValidationInterval, modelTimeIntervals);
    };
}
