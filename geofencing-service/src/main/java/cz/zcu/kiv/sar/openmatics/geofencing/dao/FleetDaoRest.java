package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.openmatics.cloud.api.asset.management.IVehicleService;
import com.openmatics.cloud.fleetmanagement.api.IFleetService;
import com.openmatics.cloud.fleetmanagement.model.FleetDetail;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FleetDaoRest implements FleetDao {

    private static final Logger LOG = LoggerFactory.getLogger(FleetDaoRest.class);

    private IFleetService fleetService;

    private IVehicleService vehicleService;

    public FleetDaoRest(IFleetService fleetService, IVehicleService vehicleService) {
        this.fleetService = fleetService;
        this.vehicleService = vehicleService;
    }

    @Override
    public Iterable<Fleet> findAll() {
        List<Fleet> fleetList = new ArrayList<>();
        List<FleetDetail> fleets = fleetService.getFleets(true, null);

        for (FleetDetail fleet : fleets) {
            List<Vehicle> vehicleList = new ArrayList<>(fleet.getAssetsList().size());

            for (String id : fleet.getAssetsList()) {
                try {
                    com.openmatics.cloud.core.api.Vehicle vehicle = vehicleService.get(id);
                    Vehicle newVehicle = new Vehicle(vehicle.getId(), vehicle.getName());
                    vehicleList.add(newVehicle);
                } catch (NotFoundException e) {
                    LOG.warn("A vehicle '" + id + "'from a fleet '" + fleet.getId() + "' not found.", e);
                }
            }

            Fleet newFleet = new Fleet(String.valueOf(fleet.getId()), fleet.getName(), vehicleList);
            fleetList.add(newFleet);
        }

        return fleetList;
    }

    @Override
    public Iterable<Fleet> findFleetByVehicleId(String vehicleId) {
        List<Fleet> fleetList = new ArrayList<>();
        List<FleetDetail> fleets = fleetService.getFleets(true, null);

        for (FleetDetail fleet : fleets) {
            List<String> assetList = fleet.getAssetsList();

            if (assetList == null) {
                continue;
            }

            if (assetList.contains(vehicleId)) {
                Fleet newFleet = new Fleet(String.valueOf(fleet.getId()), fleet.getName(), null);
                fleetList.add(newFleet);
            }
        }

        return fleetList;
    }
}
