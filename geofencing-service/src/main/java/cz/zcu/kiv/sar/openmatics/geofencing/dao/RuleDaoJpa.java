package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class RuleDaoJpa extends GenericDaoJpa<Rule> implements RuleDao {

    public RuleDaoJpa() {
        super(Rule.class);
    }

    @Override
    public List<Rule> findRulesByVehicleId(String vehicleId) {
        TypedQuery<Rule> query = em.createQuery("SELECT r FROM Rule r WHERE :vehicleId in (SELECT v.vehicleId FROM VehicleId v)", Rule.class);
        query.setParameter("vehicleId", vehicleId);

        try {
            return query.getResultList();
        } catch (NoResultException e) {
            Logger.getLogger(RuleDaoJpa.class.getName()).log(Level.ERROR, null, e);
            return null;
        }
    }

    @Override
    public List<Rule> findRulesByFleetId(String fleetId) {
        TypedQuery<Rule> query = em.createQuery("SELECT r FROM Rule r WHERE :fleetId in (SELECT f.fleetId FROM FleetId f)", Rule.class);
        query.setParameter("fleetId", fleetId);

        try {
            return query.getResultList();
        } catch (NoResultException e) {
            Logger.getLogger(RuleDaoJpa.class.getName()).log(Level.ERROR, null, e);
            return null;
        }
    }
}
