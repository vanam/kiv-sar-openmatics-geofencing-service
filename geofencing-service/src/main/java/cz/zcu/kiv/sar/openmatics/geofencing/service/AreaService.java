package cz.zcu.kiv.sar.openmatics.geofencing.service;

import com.microsoft.azure.documentdb.DocumentClientException;
import cz.zcu.kiv.sar.openmatics.geofencing.api.IAreaService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.AreaConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.PositionManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Area;
import org.springframework.stereotype.Service;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service for serving {@link Area} objects.
 * <p>
 * Date: 10.11.17
 *
 * @author Marek Zimmermann
 */
@Service
public class AreaService implements IAreaService {
    private AreaManager areaManager;

    private PositionManager positionManager;

    public AreaService(AreaManager areaManager, PositionManager positionManager) {
        this.areaManager = areaManager;
        this.positionManager = positionManager;
    }

    @Override
    public List<Area> getAreas() {
        Iterable<cz.zcu.kiv.sar.openmatics.geofencing.domain.Area> domainAreas = areaManager.findAll();

        return StreamSupport
                .stream(domainAreas.spliterator(), false)
                .map(AreaConverter.domainToModel)
                .collect(Collectors.toList());
    }

    @Override
    public Area addArea(Area area) {
        //TODO maybe check if id == null?

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area savedArea = areaManager.save(AreaConverter.modelToDomain.apply(area));

        try {
            positionManager.save(savedArea);
        } catch (DocumentClientException e) {
            e.printStackTrace();
            // TODO remove area from MS SQL
            // TODO inform about it
        }

        return AreaConverter.domainToModel.apply(savedArea);
    }

    @Override
    public Area updateArea(Area area) {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area domainArea = areaManager.findOne(area.getId());

        if (domainArea == null) {
            throw new ClientErrorException("Entity 'Area' not found by ID: '" + area.getId() + "'", Response.Status.NOT_FOUND);
        } else {
            //convert model to domain
            cz.zcu.kiv.sar.openmatics.geofencing.domain.Area convertedArea = AreaConverter.modelToDomain.apply(area);

            //update domain object with things from model
            domainArea.update(convertedArea.getName(), convertedArea.getType(), convertedArea.getPositions());

            //save domain area
            cz.zcu.kiv.sar.openmatics.geofencing.domain.Area savedArea = areaManager.save(domainArea);

            try {
                positionManager.replace(savedArea);
            } catch (DocumentClientException e) {
                e.printStackTrace();
                // TODO remove area from MS SQL
                // TODO inform about it
            }

            // return model area
            return AreaConverter.domainToModel.apply(savedArea);
        }
    }

    @Override
    public void deleteArea(String id) {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area domainArea = areaManager.findOne(id);

        if (domainArea == null) {
            throw new ClientErrorException("Entity 'Area' not found by ID: '" + id + "'", Response.Status.NOT_FOUND);
        }

        areaManager.remove(id);

        try {
            positionManager.remove(id);
        } catch (DocumentClientException e) {
            // TODO something?
        }
    }
}
