package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.RuleViolation;

import java.util.List;

public interface RuleViolationDao extends GenericDao<RuleViolation> {

    List<RuleViolation> findRuleViolationsByVehicleId(String vehicleId);

}
