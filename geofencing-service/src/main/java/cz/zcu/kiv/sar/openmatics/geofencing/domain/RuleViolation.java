package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
public class RuleViolation extends BaseObject {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @NotNull
    private Rule rule;

    @NotEmpty
    @NotNull
    private String vehicleId;

    private boolean corrected;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public RuleViolation() {
    }

    public RuleViolation(String vehicleId, Rule rule) {
        update(vehicleId, rule, false);
    }

    @Transient
    public void update(String vehicleId, Rule rule, boolean corrected) {
        String validationError = isValid(vehicleId, rule);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        setVehicleId(vehicleId);
        setRule(rule);
        setCorrected(corrected);
    }

    @Transient
    private String isValid(String vehicleId, Rule rule) {
        // Vehicle ID can not be null
        if (vehicleId == null) {
            return "Mandatory field 'vehicleId' cannot be set to null.";
        }

        // Rule can not be null
        if (rule == null) {
            return "Mandatory field 'rule' cannot be set to null.";
        }

        return null;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public boolean isCorrected() {
        return corrected;
    }

    public void setCorrected(boolean corrected) {
        this.corrected = corrected;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("rule ID", rule.getId())
                .append("vehicle ID", vehicleId)
                .append("corrected", corrected)
                .append("createDate", createDate)
                .append("updateDate", updateDate)
                .append("deleteDate", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RuleViolation that = (RuleViolation) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(corrected, that.corrected)
                .append(rule, that.rule)
                .append(vehicleId, that.vehicleId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(rule)
                .append(vehicleId)
                .append(corrected)
                .toHashCode();
    }
}
