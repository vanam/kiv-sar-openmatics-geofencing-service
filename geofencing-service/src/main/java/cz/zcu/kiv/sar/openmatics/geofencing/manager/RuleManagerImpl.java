package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleViolationDao;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Rule;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.RuleViolation;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RuleManagerImpl extends GenericManagerImpl<Rule> implements RuleManager {

    private final RuleViolationDao ruleViolationDao;

    private PositionManager positionManager;

    private FleetManager fleetManager;

    private RuleDao ruleDao;

    protected RuleManagerImpl(RuleDao ruleDao, RuleViolationDao ruleViolationDao, PositionManager positionManager,
                              FleetManager fleetManager) {
        super(ruleDao);
        this.ruleViolationDao = ruleViolationDao;
        this.positionManager = positionManager;
        this.fleetManager = fleetManager;
        this.ruleDao = ruleDao;
    }

    @Override
    public List<RuleViolation> matchRules(String vehicleId, LocalDateTime time ,double latitude, double longitude) {
        List<RuleViolation> ruleViolations = new ArrayList<>();

        List<RuleViolation> ruleViolationsByVehicleId = ruleViolationDao.findRuleViolationsByVehicleId(vehicleId);

        Map<String, RuleViolation> ruleViolationsMap = new HashMap<>(ruleViolationsByVehicleId.size());
        for (RuleViolation ruleViolation : ruleViolationsByVehicleId) {
            ruleViolationsMap.put(ruleViolation.getRule().getId(), ruleViolation);
        }

        List<String> insideArea = positionManager.getInsidePositionIds(longitude, latitude);
        List<String> outsideArea = positionManager.getOutsidePositionIds(longitude, latitude);

        List<Rule> rulesByVehicleId = findRulesByVehicleId(vehicleId);

        NEXT:
        for (Rule rule : rulesByVehicleId) {
            Rule.Type type = rule.getType();

            if (rule.isApplicableAt(time)) {
                RuleViolation ruleViolation = ruleViolationsMap.get(rule.getId());
                if (ruleViolation != null) {
                    switch (rule.getType()) {
                        case INSIDE:
                            for (Area area : rule.getAreas()) {
                                if (insideArea.contains(area.getId())) {
                                    ruleViolation.setCorrected(true);
                                    continue NEXT;
                                }
                            }

                            break;
                        case OUTSIDE:
                            for (Area area : rule.getAreas()) {
                                if (outsideArea.contains(area.getId())) {
                                    ruleViolation.setCorrected(true);
                                    continue NEXT;
                                }
                            }

                            break;
                    }
                } else {
                    switch (type) {
                        case INSIDE:
                            for (Area area : rule.getAreas()) {
                                if (outsideArea.contains(area.getId())) {
                                    ruleViolations.add(ruleViolationDao.save(new RuleViolation(vehicleId, rule)));
                                    continue NEXT;
                                }
                            }

                            break;
                        case OUTSIDE:
                            for (Area area : rule.getAreas()) {
                                if (insideArea.contains(area.getId())) {
                                    ruleViolations.add(ruleViolationDao.save(new RuleViolation(vehicleId, rule)));
                                    continue NEXT;
                                }
                            }

                            break;
                    }
                }
            }
        }


        // If rule was violated, create new RuleViolation record and return it
        // If rule was corrected (there are uncorrected RuleViolations in database which are no longer violated), correct them and return them
        return ruleViolations;
    }

    private List<Rule> findRulesByVehicleId(String vehicleId) {
        List<Rule> rulesByVehicleId = ruleDao.findRulesByVehicleId(vehicleId);

        Iterable<Fleet> fleets = fleetManager.findFleetByVehicleId(vehicleId);

        for (Fleet fleet : fleets) {
            List<Rule> rulesByFleetId = ruleDao.findRulesByFleetId(fleet.getId());
            rulesByVehicleId.addAll(rulesByFleetId);
        }

        return rulesByVehicleId;
    }

}