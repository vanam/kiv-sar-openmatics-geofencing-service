package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class PositionCosmos {

    private String id;

    private String clientId;

    private Location location;

    public PositionCosmos(String areaId, String type, List<Double[]> coordinates) {
        this.id = areaId;
        this.location = new Location(type, coordinates);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("clientId", clientId)
                .append("location", location)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PositionCosmos that = (PositionCosmos) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(clientId, that.clientId)
                .append(location, that.location)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(clientId)
                .append(location)
                .toHashCode();
    }

    private static class Location {
        private String type;

        private List<Double[]> coordinates;

        Location(String type, List<Double[]> coordinates) {
            this.type = type;
            this.coordinates = coordinates;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Double[][] getCoordinates() {
            Double[][] array = new Double[coordinates.size()][2];
            for (int i = 0; i < coordinates.size(); i++) {
                array[i] = coordinates.get(i);
            }

            return array;
        }

        public void setCoordinates(List<Double[]> coordinates) {
            this.coordinates = coordinates;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("type", type)
                    .append("coordinates", getCoordinates())
                    .toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            Location location = (Location) o;

            return new EqualsBuilder()
                    .append(type, location.type)
                    .append(getCoordinates(), location.getCoordinates())
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(type)
                    .append(getCoordinates())
                    .toHashCode();
        }
    }
}
