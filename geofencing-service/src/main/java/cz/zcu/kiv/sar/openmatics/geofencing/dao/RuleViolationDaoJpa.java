package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.RuleViolation;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class RuleViolationDaoJpa extends GenericDaoJpa<RuleViolation> implements RuleViolationDao {

    public RuleViolationDaoJpa() {
        super(RuleViolation.class);
    }

    @Override
    public List<RuleViolation> findRuleViolationsByVehicleId(String vehicleId) {
        TypedQuery<RuleViolation> query = em.createQuery("SELECT r FROM RuleViolation r WHERE :vehicleId = r.vehicleId AND r.corrected = false", RuleViolation.class);
        query.setParameter("vehicleId", vehicleId);

        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
