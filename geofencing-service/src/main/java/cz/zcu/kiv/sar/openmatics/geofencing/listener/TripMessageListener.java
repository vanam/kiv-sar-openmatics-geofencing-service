package cz.zcu.kiv.sar.openmatics.geofencing.listener;

import com.openmatics.api.common.messaging.object.Gps;
import com.openmatics.app.tripmodel.common.message.TripMessage;
import com.openmatics.cloud.lib.servicemanagement.communication.message.envelope.MessageEnvelope;
import com.openmatics.cloud.lib.servicemanagement.communication.message.receiving.MessageListener;
import com.openmatics.cloud.lib.servicemanagement.util.IServiceInformationHolder;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.RuleViolation;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.EventManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.RuleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class TripMessageListener implements MessageListener<TripMessage> {
    private static final Logger LOG = LoggerFactory.getLogger(TripMessageListener.class);

    private final RuleManager ruleManager;

    private final EventManager eventManager;

    private final IServiceInformationHolder serviceInformationHolder;

    public TripMessageListener(RuleManager ruleManager, EventManager eventManager, IServiceInformationHolder serviceInformationHolder) {
        this.ruleManager = ruleManager;
        this.eventManager = eventManager;
        this.serviceInformationHolder = serviceInformationHolder;
    }

    @Override
    public void onMessage(MessageEnvelope<TripMessage> tripMessage) {
        if (!isValidMessage(tripMessage)) {
            LOG.warn("Invalid trip message received.");
            return;
        }

        String clientId = tripMessage.getHeader().getClientId();
        String assetId = tripMessage.getHeader().getAssetId();

        tripMessage
                .getPayload()
                .getTripMessageRecords()
                .forEach(tripMessageRecord -> {

                    // Check if the message record belongs to the client running this service
                    if (!clientId.equals(serviceInformationHolder.getServiceClientId())) {
                        LOG.warn(String.format("TripMessage for unknown client '%s' received.", clientId));
                        return;
                    }

                    Gps gps = tripMessageRecord.getTripDataRecord().getPosition();

                    LocalDateTime time = LocalDateTime.ofEpochSecond(
                            tripMessageRecord.getDateTime().getMillis() / 1000,
                            tripMessageRecord.getDateTime().getMillisOfSecond(),
                            ZoneOffset.UTC
                    );

                    // Get rules which are violated
                    List<RuleViolation> violations = ruleManager.matchRules(assetId, time, gps.getLatitude(), gps.getLongitude());

                    // Generate events
                    violations.forEach(violation -> {
                        Event.EventType type;

                        if (violation.isCorrected()) {
                            type = Event.EventType.CORRECTION;
                        } else {
                            type = Event.EventType.VIOLATION;
                        }

                        eventManager.save(new Event(type, violation.getRule(), assetId, time));
                    });
                });
    }

    private boolean isValidMessage(MessageEnvelope<TripMessage> tripMessage) {
        if (tripMessage.getHeader() == null) {
            return false;
        }

        if (tripMessage.getHeader().getAssetId() == null) {
            return false;
        }

        if (tripMessage.getHeader().getClientId() == null) {
            return false;
        }

        if (tripMessage.getPayload() == null) {
            return false;
        }

        return true;
    }

}
