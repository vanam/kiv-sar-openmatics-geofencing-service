package cz.zcu.kiv.sar.openmatics.geofencing;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.openmatics.cloud.lib.rest.jersey.CommonExceptionMapper;

import javax.annotation.Priority;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(6000)
public class ExceptionMapper extends CommonExceptionMapper {

    @Override
    public Response toResponse(Throwable error) {
        if (error instanceof IllegalArgumentException) {
            // Map exception which is thrown when creating invalid domain object
            error = new ClientErrorException(
                    error.getMessage(),
                    Response.Status.BAD_REQUEST,
                    error.getCause()
            );
        } else if (error instanceof InvalidFormatException) {
            // Map exception which is thrown when jackson fails to parse json from a client
            error = new ClientErrorException(
                    error.getMessage(),
                    Response.Status.BAD_REQUEST,
                    error.getCause()
            );
        }

        return super.toResponse(error);
    }
}
