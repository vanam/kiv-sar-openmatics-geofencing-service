package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.EventDao;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EventManagerImpl extends GenericManagerImpl<Event> implements EventManager {

    protected EventManagerImpl(EventDao eventDao) {
        super(eventDao);
    }

}
