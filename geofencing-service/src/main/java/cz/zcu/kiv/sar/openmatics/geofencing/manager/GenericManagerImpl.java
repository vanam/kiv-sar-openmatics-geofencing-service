package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.GenericDao;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;

import javax.transaction.Transactional;

@Transactional
abstract public class GenericManagerImpl<T extends BaseObject> implements GenericManager<T> {

    private GenericDao<T> dao;

    protected GenericManagerImpl(GenericDao<T> dao) {
        this.dao = dao;
    }

    @Override
    public T findOne(String id) {
        return dao.findOne(id);
    }

    @Override
    public Iterable<T> findAll() {
        return dao.findAll();
    }

    @Override
    public long getCount() {
        return dao.getCount();
    }

    @Override
    public void remove(String id) {
        T item = this.findOne(id);
        dao.remove(item);
    }

    @Override
    public T save(T item) {
        return dao.save(item);
    }
}
