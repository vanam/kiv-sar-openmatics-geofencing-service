package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.AreaDao;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AreaManagerImpl extends GenericManagerImpl<Area> implements AreaManager {

    protected AreaManagerImpl(AreaDao areaDao) {
        super(areaDao);
    }

}
