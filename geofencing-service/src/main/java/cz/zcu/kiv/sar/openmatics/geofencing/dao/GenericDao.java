package cz.zcu.kiv.sar.openmatics.geofencing.dao;


import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;

/**
 * Common interface for all DAOs
 */
public interface GenericDao<T extends BaseObject> {

    T findOne(String id);

    Iterable<T> findAll();

    long getCount();

    void remove(T toRemove);

    T save(T item);

}
