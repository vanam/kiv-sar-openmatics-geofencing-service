package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.FleetDao;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import org.springframework.stereotype.Service;

@Service
public class FleetManagerImpl implements FleetManager {

    FleetDao fleetDao;

    public FleetManagerImpl(FleetDao fleetDao) {
        this.fleetDao = fleetDao;
    }

    @Override
    public Iterable<Fleet> findAll() {
        return fleetDao.findAll();
    }

    @Override
    public Iterable<Fleet> findFleetByVehicleId(String vehicleId) {
        return fleetDao.findFleetByVehicleId(vehicleId);
    }
}
