package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;

public interface VehicleManager {

    Iterable<Vehicle> findAll();
}
