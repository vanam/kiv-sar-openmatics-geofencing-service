package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.time.LocalTime;

/**
 * Entity containing information for defining time of rule evaluation.
 * <p>
 * Date: 19.11.17
 *
 * @author Marek Zimmermann
 */
@Embeddable
public class TimeInterval {
    @NotEmpty
    private LocalTime timeFrom;

    @NotEmpty
    private LocalTime timeTo;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public TimeInterval() {
    }

    public TimeInterval(LocalTime timeFrom, LocalTime timeTo) {
        String validationError = isValid(timeFrom, timeTo);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    @Transient
    private String isValid(LocalTime timeFrom, LocalTime timeTo) {
        if (timeFrom == null) {
            return "Mandatory field 'timeFrom' cannot be set to null.";
        }

        if (timeTo == null) {
            return "Mandatory field 'timeTo' cannot be set to null.";
        }

        return null;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("timeFrom", timeFrom)
                .append("timeTo", timeTo)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TimeInterval that = (TimeInterval) o;

        return new EqualsBuilder()
                .append(timeFrom, that.timeFrom)
                .append(timeTo, that.timeTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(timeFrom)
                .append(timeTo)
                .toHashCode();
    }
}
