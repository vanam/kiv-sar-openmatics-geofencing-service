package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Position;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.PositionCosmos;

import java.util.ArrayList;
import java.util.List;

public class CosmosConverter {

    public static PositionCosmos convertPolygonAreaToPositionCosmos(Area area) {
        String shape = "Polygon";
        List<Double[]> coordinates = new ArrayList<>();

        for (Position position : area.getPositions()) {
            coordinates.add(new Double[]{position.getLongitude(), position.getLatitude()});
        }
        // this shape has to start and end with same coordinates
        coordinates.add(coordinates.get(0));

        return new PositionCosmos(area.getId(), shape, coordinates);
    }

    public static PositionCosmos convertRectangleAreaToPositionCosmos(Area area) {
        String shape = "Polygon";
        List<Double[]> coordinates = new ArrayList<>();

        List<Position> positions = area.getPositions();

        // D ... C
        // .     .
        // A ... B
        Position a = positions.get(0);
        Position c = positions.get(1);

        coordinates.add(new Double[]{a.getLongitude(), a.getLatitude()}); // A
        coordinates.add(new Double[]{c.getLongitude(), a.getLatitude()}); // B
        coordinates.add(new Double[]{c.getLongitude(), c.getLatitude()}); // C
        coordinates.add(new Double[]{a.getLongitude(), c.getLatitude()}); // D

        // this shape has to start and end with same coordinates
        coordinates.add(coordinates.get(0));

        return new PositionCosmos(area.getId(), shape, coordinates);
    }

    public static PositionCosmos convertCircleAreaToPositionCosmos(Area area) {
        String shape = "Polygon";
        List<Double[]> coordinates = new ArrayList<>();

        double longitudeCenter = area.getPositions().get(0).getLongitude();
        double latitudeCenter = area.getPositions().get(0).getLatitude();

        double longitudeRadius = area.getPositions().get(1).getLongitude();
        double latitudeRadius = area.getPositions().get(1).getLatitude();

        double radius = Math.sqrt(Math.pow(longitudeRadius - longitudeCenter, 2) + Math.pow(latitudeRadius - latitudeCenter, 2));

        for (double i = 360; i >= 0; i = i - 20) {
            // generate points on circle
            double longitude = longitudeCenter + Math.sin(i / 60) * radius;
            double latitude = latitudeCenter + Math.cos(i / 60) * radius;

            coordinates.add(new Double[]{longitude, latitude});
        }

        // this shape has to start and end with same coordinates
        coordinates.add(coordinates.get(0));

        return new PositionCosmos(area.getId(), shape, coordinates);
    }

    public static PositionCosmos convertPointToPositionCosmos(double longitude, double latitude) {
        ArrayList<Double[]> coordinates = new ArrayList<>();
        coordinates.add(new Double[]{longitude, latitude});
        return new PositionCosmos(null, "Point", coordinates);
    }
}
