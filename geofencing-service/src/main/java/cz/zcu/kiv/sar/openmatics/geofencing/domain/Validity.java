package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Entity containing time information for rule evaluation.
 * <p>
 * Date: 19.11.17
 *
 * @author Marek Zimmermann
 */
@Entity
public class Validity extends BaseObject {
    @Embedded
    private ValidationInterval validationInterval;

    //lists of time intervals
    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> mondayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> tuesdayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> wednesdayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> thursdayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> fridayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> saturdayIntervals;

    @ElementCollection(targetClass = TimeInterval.class, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<TimeInterval> sundayIntervals;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public Validity() {
    }

    public Validity(ValidationInterval validationInterval, List<TimeInterval> mondayIntervals, List<TimeInterval> tuesdayIntervals, List<TimeInterval> wednesdayIntervals, List<TimeInterval> thursdayIntervals, List<TimeInterval> fridayIntervals, List<TimeInterval> saturdayIntervals, List<TimeInterval> sundayIntervals) {
        String validationError = isValid(validationInterval, mondayIntervals, tuesdayIntervals, wednesdayIntervals, thursdayIntervals, fridayIntervals, saturdayIntervals, sundayIntervals);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        this.validationInterval = validationInterval;
        this.mondayIntervals = mondayIntervals;
        this.tuesdayIntervals = tuesdayIntervals;
        this.wednesdayIntervals = wednesdayIntervals;
        this.thursdayIntervals = thursdayIntervals;
        this.fridayIntervals = fridayIntervals;
        this.saturdayIntervals = saturdayIntervals;
        this.sundayIntervals = sundayIntervals;
    }

    @Transient
    private String isValid(ValidationInterval validationInterval, List<TimeInterval> mondayIntervals, List<TimeInterval> tuesdayIntervals, List<TimeInterval> wednesdayIntervals, List<TimeInterval> thursdayIntervals, List<TimeInterval> fridayIntervals, List<TimeInterval> saturdayIntervals, List<TimeInterval> sundayIntervals) {
        // Validation interval can't be null
        if (validationInterval == null) {
            return "Mandatory field 'validationInterval' cannot be set to null.";
        }

        //all time intervals can be empty, but must not be null plus there must be at least one time interval at all
        if (mondayIntervals == null) {
            return "Mandatory field 'mondayIntervals' cannot be set to null.";
        }

        if (tuesdayIntervals == null) {
            return "Mandatory field 'tuesdayIntervals' cannot be set to null.";
        }

        if (wednesdayIntervals == null) {
            return "Mandatory field 'wednesdayIntervals' cannot be set to null.";
        }

        if (thursdayIntervals == null) {
            return "Mandatory field 'thursdayIntervals' cannot be set to null.";
        }

        if (fridayIntervals == null) {
            return "Mandatory field 'fridayIntervals' cannot be set to null.";
        }

        if (saturdayIntervals == null) {
            return "Mandatory field 'saturdayIntervals' cannot be set to null.";
        }

        if (sundayIntervals == null) {
            return "Mandatory field 'sundayIntervals' cannot be set to null.";
        }

        //no need to check how many time intervals there is - if there is none, the rule will always apply

        return null;
    }

    public ValidationInterval getValidationInterval() {
        return validationInterval;
    }

    public void setValidationInterval(ValidationInterval validationInterval) {
        this.validationInterval = validationInterval;
    }

    public List<TimeInterval> getMondayIntervals() {
        return mondayIntervals;
    }

    public void setMondayIntervals(List<TimeInterval> mondayIntervals) {
        this.mondayIntervals = mondayIntervals;
    }

    public List<TimeInterval> getTuesdayIntervals() {
        return tuesdayIntervals;
    }

    public void setTuesdayIntervals(List<TimeInterval> tuesdayIntervals) {
        this.tuesdayIntervals = tuesdayIntervals;
    }

    public List<TimeInterval> getWednesdayIntervals() {
        return wednesdayIntervals;
    }

    public void setWednesdayIntervals(List<TimeInterval> wednesdayIntervals) {
        this.wednesdayIntervals = wednesdayIntervals;
    }

    public List<TimeInterval> getThursdayIntervals() {
        return thursdayIntervals;
    }

    public void setThursdayIntervals(List<TimeInterval> thursdayIntervals) {
        this.thursdayIntervals = thursdayIntervals;
    }

    public List<TimeInterval> getFridayIntervals() {
        return fridayIntervals;
    }

    public void setFridayIntervals(List<TimeInterval> fridayIntervals) {
        this.fridayIntervals = fridayIntervals;
    }

    public List<TimeInterval> getSaturdayIntervals() {
        return saturdayIntervals;
    }

    public void setSaturdayIntervals(List<TimeInterval> saturdayIntervals) {
        this.saturdayIntervals = saturdayIntervals;
    }

    public List<TimeInterval> getSundayIntervals() {
        return sundayIntervals;
    }

    public void setSundayIntervals(List<TimeInterval> sundayIntervals) {
        this.sundayIntervals = sundayIntervals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("validationInterval", validationInterval)
                .append("mondayIntervals", mondayIntervals)
                .append("tuesdayIntervals", tuesdayIntervals)
                .append("wednesdayIntervals", wednesdayIntervals)
                .append("thursdayIntervals", thursdayIntervals)
                .append("fridayIntervals", fridayIntervals)
                .append("saturdayIntervals", saturdayIntervals)
                .append("sundayIntervals", sundayIntervals)
                .append("createDate", createDate)
                .append("updateDate", updateDate)
                .append("deleteDate", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Validity validity = (Validity) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(validationInterval, validity.validationInterval)
                .append(mondayIntervals, validity.mondayIntervals)
                .append(tuesdayIntervals, validity.tuesdayIntervals)
                .append(wednesdayIntervals, validity.wednesdayIntervals)
                .append(thursdayIntervals, validity.thursdayIntervals)
                .append(fridayIntervals, validity.fridayIntervals)
                .append(saturdayIntervals, validity.saturdayIntervals)
                .append(sundayIntervals, validity.sundayIntervals)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(validationInterval)
                .append(mondayIntervals)
                .append(tuesdayIntervals)
                .append(wednesdayIntervals)
                .append(thursdayIntervals)
                .append(fridayIntervals)
                .append(saturdayIntervals)
                .append(sundayIntervals)
                .toHashCode();
    }
}
