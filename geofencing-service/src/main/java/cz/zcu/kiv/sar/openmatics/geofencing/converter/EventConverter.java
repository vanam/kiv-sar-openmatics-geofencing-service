package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.configuration.ContextConfiguration;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;

import java.time.ZoneOffset;
import java.util.function.Function;

public class EventConverter {
    public static final Function<Event, cz.zcu.kiv.sar.openmatics.geofencing.model.Event> domainToModel = event -> {
        if (event == null) {
            return null;
        }

        Event.EventType type = event.getType();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event.EventType eventType;
        if (type == null) {
            eventType = null;
        } else {
            switch (type) {
                default:
                case VIOLATION:
                    eventType = cz.zcu.kiv.sar.openmatics.geofencing.model.Event.EventType.VIOLATION;
                    break;
                case CORRECTION:
                    eventType = cz.zcu.kiv.sar.openmatics.geofencing.model.Event.EventType.CORRECTION;
                    break;
            }
        }

        long epochSecond;
        if (event.getEventTime() == null) {
            epochSecond = Long.MIN_VALUE;
        } else {
            epochSecond = event.getEventTime()
                    .toEpochSecond(ZoneOffset.of(ContextConfiguration.DEFAULT_TIME_ZONE_OFFSET));
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.model.Event(event.getId(), eventType,
                RuleConverter.domainToModel(event.getRule()), event.getVehicleId(), epochSecond);
    };
}
