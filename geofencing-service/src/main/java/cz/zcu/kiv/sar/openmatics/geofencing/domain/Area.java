package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Area extends BaseObject {

    @NotEmpty
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AreaType type;

    @NotEmpty
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "area_id")
    @OrderBy("order")
    private List<Position> positions;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public Area() {
    }

    public Area(String name, AreaType type, List<Position> positions) {
        update(name, type, positions);
    }

    @Transient
    public void update(String name, AreaType type, List<Position> positions) {
        String validationError = isValid(name, type, positions);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        setName(name);
        setType(type);
        setPositions(positions);
    }

    @Transient
    private String isValid(String name, AreaType type, List<Position> positions) {
        // Name must not be empty
        if (name == null || name.length() == 0) {
            return "Mandatory field 'name' cannot be empty.";
        }

        // Type must be defined
        if (type == null) {
            return "Mandatory field 'type' cannot be set to null.";
        }

        // We need at least two points
        if (positions == null || positions.size() < 2) {
            return "Mandatory field 'positions' must have at least two coordinates.";
        }

        // Type dependent checks
        switch (type) {
            case RECTANGLE:
            case CIRCLE:
                // Circle/rectangle is defined using two points
                if (positions.size() > 2) {
                    return "Mandatory field 'positions' must have exactly two coordinates for rectangular/circular areas.";
                }

                break;
            case POLYGON:
                // Polygon is defined using at least three points
                if (positions.size() < 3) {
                    return "Mandatory field 'positions' must have at least three coordinates for polygon area.";
                }

                break;
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AreaType getType() {
        return type;
    }

    public void setType(AreaType type) {
        this.type = type;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("type", type)
                .append("createDate", createDate)
                .append("updateDate", updateDate)
                .append("deleteDate", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(name, area.name)
                .append(type, area.type)
                .append(positions, area.positions)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(name)
                .append(type)
                .append(positions)
                .toHashCode();
    }

    public enum AreaType {
        POLYGON, RECTANGLE, CIRCLE
    }
}
