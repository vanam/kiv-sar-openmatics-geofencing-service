package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IVehicleService;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.VehicleManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for serving {@link Vehicle} objects.
 * <p>
 * Date: 10.11.17
 *
 * @author Marek Zimmermann
 */
@Service
public class VehicleService implements IVehicleService {
    private VehicleManager vehicleManager;

    public VehicleService(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return (List<Vehicle>) vehicleManager.findAll();
    }
}
