package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;

@Entity
public class Event extends BaseObject {

    @NotNull
    @Enumerated(EnumType.STRING)
    private EventType type;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Rule rule;

    @NotEmpty
    private String vehicleId;

    @NotNull
    private LocalDateTime eventTime;

    /**
     * Do not use this constructor! It is for JPA only.
     */
    @Deprecated
    public Event() {
    }

    public Event(EventType type, Rule rule, String vehicleId, LocalDateTime eventTime) {
        String validationError = isValid(type, rule, vehicleId, eventTime);

        if (validationError != null) {
            throw new IllegalArgumentException(validationError);
        }

        this.type = type;
        this.rule = rule;
        this.vehicleId = vehicleId;
        this.eventTime = eventTime;
    }

    @Transient
    private String isValid(EventType type, Rule rule, String vehicleId, LocalDateTime eventTime) {
        //type must not be null
        if (type == null) {
            return "Mandatory field 'type' cannot be set to null.";
        }

        //rule must not be null
        if (rule == null) {
            return "Mandatory field 'rule' cannot be set to null.";
        }
        //eventTime must not be null
        if (eventTime == null) {
            return "Mandatory field 'eventTime' cannot be set to null.";
        }

        //no need to check rule validity, rule is checked in its own constructor

        if (StringUtils.isBlank(vehicleId)) {
            return "Mandatory field 'vehicleId' cannot be empty.";
        }

        return null;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }

    @Transient
    public Instant getInstant() {
        return createDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("type", type)
                .append("rule ID", rule.getId())
                .append("vehicle ID", vehicleId)
                .append("eventTime", eventTime)
                .append("createInstant", createDate)
                .append("updateInstant", updateDate)
                .append("deleteInstant", deleteDate)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(type, event.type)
                .append(rule, event.rule)
                .append(vehicleId, event.vehicleId)
                .append(eventTime, event.eventTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(type)
                .append(rule)
                .append(vehicleId)
                .append(eventTime)
                .toHashCode();
    }

    public enum EventType {
        VIOLATION, CORRECTION
    }
}
