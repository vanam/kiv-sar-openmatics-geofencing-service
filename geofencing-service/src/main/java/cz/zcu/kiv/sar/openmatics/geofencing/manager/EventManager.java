package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Event;

public interface EventManager extends GenericManager<Event> {

}
