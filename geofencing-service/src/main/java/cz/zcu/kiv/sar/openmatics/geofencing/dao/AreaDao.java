package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;

public interface AreaDao extends GenericDao<Area> {

}
