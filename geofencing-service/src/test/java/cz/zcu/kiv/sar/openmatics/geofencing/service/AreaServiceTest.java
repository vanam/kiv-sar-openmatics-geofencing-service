package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IAreaService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.AreaConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Position;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.PositionManager;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import javax.ws.rs.ClientErrorException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AreaServiceTest {

    private AreaManager areaManager;

    private PositionManager positionManager;

    private IAreaService areaService;

    @BeforeEach
    void setUp(@Mock AreaManager areaManager, @Mock PositionManager positionManager) {
        this.areaManager = areaManager;
        this.positionManager = positionManager;

        areaService = new AreaService(areaManager, positionManager);
    }

    @Test
    @DisplayName("Get all areas")
    void testGetAreas() {
        Area area = new Area(
                "Rectangle area",
                Area.AreaType.RECTANGLE,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
        );
        area.setId("area1");
        area.setCreateDate(Instant.now());

        Area area2 = new Area(
                "Polygon area",
                Area.AreaType.POLYGON,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1), new Position(0, 0, 2))
        );
        area2.setId("area2");
        area2.setCreateDate(Instant.now());

        List<Area> list = new ArrayList<>();
        list.add(area);
        list.add(area2);

        when(areaManager.findAll()).thenReturn(list);
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> expectedList = list.stream()
                .map(AreaConverter.domainToModel)
                .collect(Collectors.toList());
        assertEquals(expectedList, areaService.getAreas());
    }


    @Test
    @DisplayName("Add new area")
    void testAddArea() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
        modelArea.setName("area");
        modelArea.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Area.AreaType.RECTANGLE);
        modelArea.setPositionList(Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0), new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0)));

        Area expectedArea = AreaConverter.modelToDomain.apply(modelArea);
        expectedArea.setId("area 1");
        expectedArea.setCreateDate(Instant.now());

        when(areaManager.save(any(Area.class))).thenReturn(expectedArea);
        assertEquals(AreaConverter.domainToModel.apply(expectedArea), areaService.addArea(modelArea));
    }

    @Test
    @DisplayName("Add area with invalid position list")
    void testAddArea2() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    modelArea.setName("area");
                    modelArea.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Area.AreaType.RECTANGLE);
                    modelArea.setPositionList(Lists.emptyList());

                    areaService.addArea(modelArea);
                });
    }

    @Test
    @DisplayName("Add area with invalid type")
    void testAddArea3() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    modelArea.setName("area");
                    modelArea.setPositionList(Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0), new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0)));

                    areaService.addArea(modelArea);
                });
    }

    @Test
    @DisplayName("Update area")
    void testUpdateArea() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
        modelArea.setId("area 1");
        modelArea.setName("area");
        modelArea.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Area.AreaType.RECTANGLE);
        modelArea.setPositionList(Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0), new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0)));

        Area oldArea = new Area(
                "old area",
                Area.AreaType.CIRCLE,
                Arrays.asList(new Position(0, 0, 1), new Position(0, 0, 2))
        );
        oldArea.setId("area 1");
        oldArea.setCreateDate(Instant.MIN);

        Area expectedArea = AreaConverter.modelToDomain.apply(modelArea);
        expectedArea.setId("area 1");
        expectedArea.setCreateDate(Instant.MIN);
        expectedArea.setUpdateDate(Instant.now());

        when(areaManager.findOne("area 1")).thenReturn(oldArea);
        when(areaManager.save(any(Area.class))).thenReturn(expectedArea);
        assertEquals(AreaConverter.domainToModel.apply(expectedArea), areaService.updateArea(modelArea));
    }

    @Test
    @DisplayName("Update non-existing area")
    void testUpdateArea2() {
        assertThrows(ClientErrorException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();

                    areaService.updateArea(modelArea);
                });
    }

    @Test
    @DisplayName("Update area with invalid position list")
    void testUpdateArea3() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    modelArea.setId("area 1");
                    modelArea.setName("area");
                    modelArea.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Area.AreaType.RECTANGLE);
                    modelArea.setPositionList(Lists.emptyList());

                    Area expectedArea = AreaConverter.modelToDomain.apply(modelArea);
                    expectedArea.setId("area 1");
                    expectedArea.setCreateDate(Instant.now());
                    expectedArea.setUpdateDate(Instant.now());

                    when(areaManager.findOne("area 1")).thenReturn(expectedArea);
                    areaService.updateArea(modelArea);
                });
    }

    @Test
    @DisplayName("Update area with invalid type")
    void testUpdateArea4() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area modelArea = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    modelArea.setId("area 1");
                    modelArea.setName("area");
                    modelArea.setPositionList(Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0), new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(0, 0)));

                    Area expectedArea = AreaConverter.modelToDomain.apply(modelArea);
                    expectedArea.setId("area 1");
                    expectedArea.setCreateDate(Instant.now());
                    expectedArea.setUpdateDate(Instant.now());

                    when(areaManager.findOne("area 1")).thenReturn(expectedArea);
                    areaService.updateArea(modelArea);
                });
    }

    @Test
    @DisplayName("Delete non-existing area")
    void testDeleteArea() {
        assertThrows(ClientErrorException.class,
                () -> areaService.deleteArea("area 1"));
    }
}