package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDaoConfig.class)
@Transactional
class RuleDaoJpaTest extends GenericDaoJpaTest {

    private AreaDaoJpa areaDao;

    @BeforeEach
    void setUp(@Autowired RuleDaoJpa ruleDao, @Autowired AreaDaoJpa areaDao) {
        this.areaDao = areaDao;
        this.dao = ruleDao;

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                new ArrayList<TimeInterval>() {{
                    add(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30)));
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        item = new Rule(
                "test rule 1",
                "desc",
                validity,
                new ArrayList<Area>() {{
                    add(getTestArea());
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE
        );

        item2 = new Rule(
                "test rule 1",
                "desc",
                validity,
                new ArrayList<Area>() {{
                    add(getTestArea());
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE
        );
    }

    private Area getTestArea() {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );

        areaDao.save(area);

        return area;
    }
}