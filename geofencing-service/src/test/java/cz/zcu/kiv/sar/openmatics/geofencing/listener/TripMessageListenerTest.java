package cz.zcu.kiv.sar.openmatics.geofencing.listener;

import com.openmatics.api.common.messaging.object.Gps;
import com.openmatics.app.tripmodel.common.message.TripDataRecord;
import com.openmatics.app.tripmodel.common.message.TripMessage;
import com.openmatics.app.tripmodel.common.message.TripMessageRecord;
import com.openmatics.app.tripmodel.common.message.constants.EventType;
import com.openmatics.cloud.lib.servicemanagement.communication.message.envelope.MessageEnvelope;
import com.openmatics.cloud.lib.servicemanagement.communication.message.envelope.MessageHeader;
import com.openmatics.cloud.lib.servicemanagement.util.IServiceInformationHolder;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.EventManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.RuleManager;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.time.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TripMessageListenerTest {

    private RuleManager ruleManager;

    private EventManager eventManager;

    private IServiceInformationHolder serviceInformationHolder;

    private TripMessageListener tripMessageListener;

    @BeforeEach
    void setUp(@Mock RuleManager ruleManager, @Mock EventManager eventManager, @Mock IServiceInformationHolder serviceInformationHolder) {
        this.ruleManager = ruleManager;
        this.eventManager = eventManager;
        this.serviceInformationHolder = serviceInformationHolder;

        this.tripMessageListener = new TripMessageListener(ruleManager, eventManager, serviceInformationHolder);
    }

    @Test
    @DisplayName("Test generated events")
    void testGeneratedEvents() {
        String clientId = "clientId123456789";
        String vehicleId = "veh123";

        when(serviceInformationHolder.getServiceClientId()).thenReturn(clientId);

        RuleViolation violation = new RuleViolation(vehicleId, getTestRule(8));
        RuleViolation violation2 = new RuleViolation(vehicleId, getTestRule(13));
        violation2.setCorrected(true);

        List<RuleViolation> violationList = new ArrayList<>();
        violationList.add(violation);
        violationList.add(violation2);

        when(ruleManager.matchRules(vehicleId, LocalDateTime.ofEpochSecond(1235, 678, ZoneOffset.UTC), 0.0, 0.0)).thenReturn(violationList);

        tripMessageListener.onMessage(getTestMessage(clientId, vehicleId));

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventManager, times(2)).save(captor.capture());

        List<Event> events = captor.getAllValues();

        Event expectedEvent = new Event(Event.EventType.VIOLATION, violation.getRule(), vehicleId, LocalDateTime.ofEpochSecond(1235, 678, ZoneOffset.UTC));
        Event expectedEvent2 = new Event(Event.EventType.CORRECTION, violation2.getRule(), vehicleId, LocalDateTime.ofEpochSecond(1235, 678, ZoneOffset.UTC));

        assertEquals(expectedEvent, events.get(0));
        assertEquals(expectedEvent2, events.get(1));
    }

    @Test
    @DisplayName("Test generated events for different time")
    void testGeneratedEvents2() {
        String clientId = "clientId123456789";
        String vehicleId = "veh123";

        when(serviceInformationHolder.getServiceClientId()).thenReturn(clientId);

        RuleViolation violation = new RuleViolation(vehicleId, getTestRule(8));
        RuleViolation violation2 = new RuleViolation(vehicleId, getTestRule(13));
        violation2.setCorrected(true);

        List<RuleViolation> violationList = new ArrayList<>();
        violationList.add(violation);
        violationList.add(violation2);

        when(ruleManager.matchRules(vehicleId, LocalDateTime.MAX, 0.0, 0.0)).thenReturn(violationList);

        tripMessageListener.onMessage(getTestMessage(clientId, vehicleId));

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventManager, times(0)).save(captor.capture());
    }

    @Test
    @DisplayName("Test message with no trip message records")
    void testIncompleteTripMessage() {
        String clientId = "clientId123456789";
        String vehicleId = "veh123";

        when(serviceInformationHolder.getServiceClientId()).thenReturn(clientId);

        tripMessageListener.onMessage(getTestMessageWithEmptyTripMessageRecords(clientId, vehicleId));

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventManager, times(0)).save(captor.capture());
    }

    @Test
    @DisplayName("Test message with no payload")
    void testIncompleteTripMessage2() {
        String clientId = "clientId123456789";
        String vehicleId = "veh123";

        tripMessageListener.onMessage(getTestMessageWithNoPayload(clientId, vehicleId));
    }

    @Test
    @DisplayName("Test message with no header")
    void testIncompleteTripMessage3() {
        tripMessageListener.onMessage(getTestMessageWithNoHeader());
    }

    @Test
    @DisplayName("Test message with no clientId")
    void testIncompleteTripMessage4() {
        String vehicleId = "veh123";

        tripMessageListener.onMessage(getTestMessageWithNoClientId(vehicleId));
    }


    @Test
    @DisplayName("Test message with no vehicleId")
    void testIncompleteTripMessage5() {
        String clientId = "clientId123456789";

        tripMessageListener.onMessage(getTestMessageWithNoVehicleId(clientId));
    }

    private MessageEnvelope<TripMessage> getTestMessageWithNoHeader() {
        TripMessage tripMessage = new TripMessage();
        tripMessage.addTripMessageRecord(
                new TripMessageRecord(
                        new DateTime(1235678L),
                        123L,
                        EventType.IGNITION_ON,
                        null,
                        new TripDataRecord(
                                new Gps(
                                        123L,
                                        0.0f,
                                        (byte) 100,
                                        0.0,
                                        0.0,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f
                                ),
                                123L,
                                500.f
                        )
                )
        );

        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setPayload(tripMessage);

        return envelope;
    }

    private MessageEnvelope<TripMessage> getTestMessageWithNoPayload(String clientId, String vehicleId) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setClientId(clientId);
        messageHeader.setAssetId(vehicleId);

        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setHeader(messageHeader);

        return envelope;
    }

    private MessageEnvelope<TripMessage> getTestMessageWithEmptyTripMessageRecords(String clientId, String vehicleId) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setClientId(clientId);
        messageHeader.setAssetId(vehicleId);

        TripMessage tripMessage = new TripMessage();
        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setHeader(messageHeader);
        envelope.setPayload(tripMessage);

        return envelope;
    }

    private MessageEnvelope<TripMessage> getTestMessageWithNoClientId(String vehicleId) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setAssetId(vehicleId);

        TripMessage tripMessage = new TripMessage();
        tripMessage.addTripMessageRecord(
                new TripMessageRecord(
                        new DateTime(1235678L),
                        123L,
                        EventType.IGNITION_ON,
                        null,
                        new TripDataRecord(
                                new Gps(
                                        123L,
                                        0.0f,
                                        (byte) 100,
                                        0.0,
                                        0.0,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f
                                ),
                                123L,
                                500.f
                        )
                )
        );

        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setHeader(messageHeader);
        envelope.setPayload(tripMessage);

        return envelope;
    }

    private MessageEnvelope<TripMessage> getTestMessageWithNoVehicleId(String clientId) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setClientId(clientId);

        TripMessage tripMessage = new TripMessage();
        tripMessage.addTripMessageRecord(
                new TripMessageRecord(
                        new DateTime(1235678L),
                        123L,
                        EventType.IGNITION_ON,
                        null,
                        new TripDataRecord(
                                new Gps(
                                        123L,
                                        0.0f,
                                        (byte) 100,
                                        0.0,
                                        0.0,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f
                                ),
                                123L,
                                500.f
                        )
                )
        );

        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setHeader(messageHeader);
        envelope.setPayload(tripMessage);

        return envelope;
    }

    private MessageEnvelope<TripMessage> getTestMessage(String clientId, String vehicleId) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setClientId(clientId);
        messageHeader.setAssetId(vehicleId);

        TripMessage tripMessage = new TripMessage();
        tripMessage.addTripMessageRecord(
                new TripMessageRecord(
                        new DateTime(1235678L),
                        123L,
                        EventType.IGNITION_ON,
                        null,
                        new TripDataRecord(
                                new Gps(
                                        123L,
                                        0.0f,
                                        (byte) 100,
                                        0.0,
                                        0.0,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f,
                                        0.0f
                                ),
                                123L,
                                500.f
                        )
                )
        );

        MessageEnvelope<TripMessage> envelope = new MessageEnvelope<>();

        envelope.setHeader(messageHeader);
        envelope.setPayload(tripMessage);

        return envelope;
    }

    private Rule getTestRule(int i) {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(3, 4, 1)
                                )
                );
        area.setId("area 1");
        area.setCreateDate(Instant.now());

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        return new Rule(
                "test rule " + i,
                "desc " + i * i,
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                i % 2 == 0 ? Rule.Type.INSIDE : Rule.Type.OUTSIDE);
    }
}
