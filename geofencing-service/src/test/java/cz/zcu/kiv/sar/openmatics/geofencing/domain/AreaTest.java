package cz.zcu.kiv.sar.openmatics.geofencing.domain;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AreaTest {

    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Validate javax validation constraints")
    void testValidateEntity() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Area area = new Area(
                "Rectangle area",
                Area.AreaType.RECTANGLE,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
        );

        Set<ConstraintViolation<Area>> violations = validator.validate(area);
        assertTrue(violations.isEmpty());
    }

    @Test
    @DisplayName("Construct rectangle area")
    void testConstructRectangleArea() {
        Area area = new Area(
                "Rectangle area",
                Area.AreaType.RECTANGLE,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
        );

        entityManager.persist(area);

        Area dbArea = entityManager
                .createQuery("select a from Area a", Area.class)
                .getSingleResult();

        assertEquals(area, dbArea);
    }

    @Test
    @DisplayName("Construct polygon area")
    void testConstructPolygonArea() {
        Area area = new Area(
                "Polygon area",
                Area.AreaType.POLYGON,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1), new Position(0, 0, 2))
        );

        entityManager.persist(area);

        Area dbArea = entityManager
                .createQuery("select a from Area a", Area.class)
                .getSingleResult();

        assertEquals(area, dbArea);
    }

    @Test
    @DisplayName("Construct circle area")
    void testConstructCircleArea() {
        Area area = new Area(
                "Circle area",
                Area.AreaType.CIRCLE,
                Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
        );

        entityManager.persist(area);

        Area dbArea = entityManager
                .createQuery("select a from Area a", Area.class)
                .getSingleResult();

        assertEquals(area, dbArea);
    }

    @Test
    @DisplayName("Test null name")
    void testNullName() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        null,
                        Area.AreaType.RECTANGLE,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
                )
        );
    }

    @Test
    @DisplayName("Test empty name")
    void testInvalidName() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
                )
        );
    }

    @Test
    @DisplayName("Test null type")
    void testNullType() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        null,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
                )
        );
    }

    @Test
    @DisplayName("Test null position list")
    void testNullPositionsList() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.RECTANGLE,
                        null
                )
        );
    }

    @Test
    @DisplayName("Test empty position list")
    void testEmptyPositionsList() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.RECTANGLE,
                        new ArrayList<>()
                )
        );
    }

    @Test
    @DisplayName("Test invalid position list")
    void testInvalidPositionsList1() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.RECTANGLE,
                        Collections.singletonList(new Position(0, 0, 0))
                )
        );
    }

    @Test
    @DisplayName("Test invalid position list for rectangle area")
    void testInvalidPositionsList2() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1), new Position(0, 0, 2))
                )
        );
    }

    @Test
    @DisplayName("Test invalid position list for circle area")
    void testInvalidPositionsList3() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.CIRCLE,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1), new Position(0, 0, 2))
                )
        );
    }

    @Test
    @DisplayName("Test invalid position list for polygon area")
    void testInvalidPositionsList4() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Area(
                        "name",
                        Area.AreaType.POLYGON,
                        Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1))
                )
        );
    }
}