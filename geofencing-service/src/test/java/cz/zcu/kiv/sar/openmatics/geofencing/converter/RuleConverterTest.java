package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class RuleConverterTest {
    private AreaManager areaManager;

    @BeforeEach
    void setUp(@Mock AreaManager areaManager) {
        this.areaManager = areaManager;
    }

    @Test
    @DisplayName("Test conversion from model to domain class")
    void testConvertModelToDomain() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Rule domainRule = RuleConverter.modelToDomain(rule, null, areaManager);

        Assertions.assertNotNull(domainRule);
    }

    @Test
    @DisplayName("Test conversion from model with null name to domain class")
    void testConvertModelToDomainNullName() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setName(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null description to domain class")
    void testConvertModelToDomainNullDescription() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setDescription(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null validity to domain class")
    void testConvertModelToDomainNullValidity() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setValidity(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null areas to domain class")
    void testConvertModelToDomainNullAreas() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setAreaList(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null vehicle list to domain class")
    void testConvertModelToDomainNullVehicles() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setVehicleIdList(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null fleet list to domain class")
    void testConvertModelToDomainNullFleets() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setFleetIdList(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from model with null type to domain class")
    void testConvertModelToDomainNullType() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = getTestModelAreas();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule rule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule(
                "test-rule-id",
                "test rule",
                "test rule description",
                modelValidity,
                modelAreas, Collections.emptyList(), Collections.emptyList(),
                cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.INSIDE
        );

        rule.setType(null);

        Mockito.when(areaManager.findOne("test-area-id"))
                .thenReturn(AreaConverter.modelToDomain.apply(modelAreas.get(0)));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> RuleConverter.modelToDomain(rule, null, areaManager));
    }

    @Test
    @DisplayName("Test conversion from domain to model")
    void testConvertDomainToModel() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
    }

    @Test
    @DisplayName("Test conversion from domain to model with null instance")
    void testConvertDomainToModelNull() {
        Assertions.assertNull(RuleConverter.domainToModel(null));
    }

    @Test
    @DisplayName("Test conversion from domain to model with null name")
    void testConvertDomainToModelNullName() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setName(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getName());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null description")
    void testConvertDomainToModelNullDescription() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setDescription(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getDescription());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null validity")
    void testConvertDomainToModelNullValidity() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setValidity(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getValidity());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null areas")
    void testConvertDomainToModelNullAreas() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setAreas(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getAreaList());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null vehicle list")
    void testConvertDomainToModelNullVehicles() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setVehicles(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getVehicleIdList());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null fleet list")
    void testConvertDomainToModelNullFleets() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setFleets(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getFleetIdList());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null rule type")
    void testConvertDomainToModelNullType() {
        Validity validity = getTestDomainValidity();
        Area area = getTestDomainArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        rule.setType(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = RuleConverter.domainToModel(rule);

        Assertions.assertNotNull(modelRule);
        Assertions.assertNull(modelRule.getType());
    }

    private Area getTestDomainArea() {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );

        area.setId("test-id");
        area.setCreateDate(Instant.now());
        return area;
    }

    private Validity getTestDomainValidity() {
        return new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>()
        );
    }

    private cz.zcu.kiv.sar.openmatics.geofencing.model.Validity getTestModelValidity() {
        LocalDate date1 = LocalDate.of(2001, 1, 1);
        LocalDate date2 = LocalDate.of(2020, 12, 31);

        List<cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval>[] timeIntervals = new List[7];
        for (int i = 0; i < timeIntervals.length; i++) {
            timeIntervals[i] = new ArrayList<>();
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.model.Validity(
                new cz.zcu.kiv.sar.openmatics.geofencing.model.ValidationInterval(date1.toEpochDay(),
                        date2.toEpochDay()), timeIntervals);
    }

    private List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> getTestModelAreas() {
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Position> modelPositions = new ArrayList<>();
        modelPositions.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(12, 13));
        modelPositions.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Position(13, 14));
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Area> modelAreas = new ArrayList<>();
        modelAreas.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Area("test-area-id", "test area",
                cz.zcu.kiv.sar.openmatics.geofencing.model.Area.AreaType.RECTANGLE, modelPositions,
                Instant.now().toEpochMilli()));
        return modelAreas;
    }
}
