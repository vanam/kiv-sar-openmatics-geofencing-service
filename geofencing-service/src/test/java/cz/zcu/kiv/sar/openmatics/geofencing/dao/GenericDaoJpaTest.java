package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Transactional
abstract class GenericDaoJpaTest<T extends BaseObject> {

    protected GenericDaoJpa<T> dao;

    protected T item;

    protected T item2;

    @Test
    void save() {
        dao.save(item);

        T dbArea = dao.findAll().iterator().next();

        assertEquals(item, dbArea);
    }

    @Test
    void findOne() {
        dao.save(item);

        assertEquals(item, dao.findOne(item.getId()));
    }

    @Test
    void findAll() {
        // Insert first
        dao.save(item);

        // Insert second
        dao.save(item2);

        List<T> items = new ArrayList<>();
        items.add(item);
        items.add(item2);

        List<T> dbItems = Lists.newArrayList(dao.findAll());

        assertEquals(items, dbItems);
    }

    @Test
    void getCount() {
        // Insert first
        dao.save(item);

        // Insert second
        dao.save(item2);

        assertEquals(2, dao.getCount());
    }

    @Test
    void remove() {
        // Add item
        dao.save(item);

        String id = item.getId();

        // Check its presence
        assertEquals(item, dao.findOne(id));

        // Remove item
        dao.remove(item);

        // Check its presence
        assertNull(dao.findOne(id));
    }
}