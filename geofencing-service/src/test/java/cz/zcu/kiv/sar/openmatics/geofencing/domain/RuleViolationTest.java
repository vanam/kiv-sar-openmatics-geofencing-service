package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RuleViolationTest {

    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Create violation")
    void testCreateViolation() {
        RuleViolation ruleViolation = new RuleViolation("vehicle id 1", getTestRule());

        entityManager.persist(ruleViolation);

        RuleViolation dbRuleViolation = entityManager.createQuery("select r from RuleViolation r", RuleViolation.class).getSingleResult();

        assertEquals(ruleViolation, dbRuleViolation);
    }

    @Test
    @DisplayName("Test null name")
    void testNullName() {
        assertThrows(IllegalArgumentException.class,
                () -> new RuleViolation(null, getTestRule()));
    }

    @Test
    @DisplayName("Test null rule")
    void testNullRule() {
        assertThrows(IllegalArgumentException.class,
                () -> new RuleViolation("vehicle id 1", null));
    }

    private Rule getTestRule() {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE);

        entityManager.persist(area);
        entityManager.persist(rule);

        return rule;
    }
}