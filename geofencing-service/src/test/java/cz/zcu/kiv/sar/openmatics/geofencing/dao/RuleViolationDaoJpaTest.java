package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDaoConfig.class)
@Transactional
class RuleViolationDaoJpaTest extends GenericDaoJpaTest {

    private AreaDaoJpa areaDao;

    private RuleDaoJpa ruleDao;

    @BeforeEach
    void setUp(@Autowired RuleViolationDaoJpa ruleViolationDao, @Autowired AreaDaoJpa areaDao, @Autowired RuleDaoJpa ruleDao) {
        this.areaDao = areaDao;
        this.ruleDao = ruleDao;
        this.dao = ruleViolationDao;

        item = new RuleViolation(
                "id 1",
                getTestRule()
        );

        item2 = new RuleViolation(
                "id 2",
                getTestRule()
        );
    }

    private Rule getTestRule() {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE);

        areaDao.save(area);
        ruleDao.save(rule);

        return rule;
    }
}