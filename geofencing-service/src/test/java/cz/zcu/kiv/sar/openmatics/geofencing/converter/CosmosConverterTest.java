package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Position;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.PositionCosmos;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CosmosConverterTest {

    @Test
    void convertPolygonAreaToPositionCosmos() {
        List<Position> positions = new ArrayList<>();
        List<Double[]> coordinates = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            positions.add(new Position(10 - i, 10 + i, i));
            coordinates.add(new Double[]{Double.valueOf(10 + i), Double.valueOf(10 - i)});
        }
        coordinates.add(coordinates.get(0));

        Area area = new Area("test", Area.AreaType.POLYGON, positions);
        area.setId("1");
        PositionCosmos positionCosmos = new PositionCosmos("1", "Polygon", coordinates);

        assertEquals(positionCosmos, CosmosConverter.convertPolygonAreaToPositionCosmos(area));
    }

    @Test
    void convertRectangleAreaToPositionCosmos() {
        List<Position> positions = new ArrayList<>();
        List<Double[]> coordinates = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            positions.add(new Position(9 + i, 10 + i, i));
        }
        coordinates.add(new Double[]{Double.valueOf(10), Double.valueOf(9)});
        coordinates.add(new Double[]{Double.valueOf(11), Double.valueOf(9)});
        coordinates.add(new Double[]{Double.valueOf(11), Double.valueOf(10)});
        coordinates.add(new Double[]{Double.valueOf(10), Double.valueOf(10)});
        coordinates.add(new Double[]{Double.valueOf(10), Double.valueOf(9)});

        Area area = new Area("test", Area.AreaType.RECTANGLE, positions);
        area.setId("1");
        PositionCosmos positionCosmos = new PositionCosmos("1", "Polygon", coordinates);

        assertEquals(positionCosmos, CosmosConverter.convertRectangleAreaToPositionCosmos(area));
    }

    @Test
    void convertCircleAreaToPositionCosmos() {
        List<Position> positions = new ArrayList<>();
        List<Double[]> coordinates = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            positions.add(new Position(10 + i, 10 - i, i));
        }

        coordinates.add(new Double[]{9.604846812909845, 11.35788584156861});
        coordinates.add(new Double[]{9.18230420478492, 11.153851631054694});
        coordinates.add(new Double[]{8.84977854376847, 10.822794385994836});
        coordinates.add(new Double[]{8.643876285482607, 10.401158909814487});
        coordinates.add(new Double[]{8.587264408040818, 9.935361410815657});
        coordinates.add(new Double[]{8.686175092661612, 9.476679722486052});
        coordinates.add(new Double[]{8.929719647097727, 9.075608326416022});
        coordinates.add(new Double[]{9.291087199380005, 8.776299611376578});
        coordinates.add(new Double[]{9.730496202347677, 8.611703308708483});
        coordinates.add(new Double[]{10.199573829320464, 8.59993918466005});
        coordinates.add(new Double[]{10.646681150290334, 8.742302305854395});
        coordinates.add(new Double[]{11.022597860714846, 9.023120470446115});
        coordinates.add(new Double[]{11.285940753247836, 9.411479499816371});
        coordinates.add(new Double[]{11.407719433946651, 9.864626460159597});
        coordinates.add(new Double[]{11.374527761892441, 10.332676166544823});
        coordinates.add(new Double[]{11.190019679058771, 10.76410284874018});
        coordinates.add(new Double[]{10.874506962063203, 11.111412422687001});
        coordinates.add(new Double[]{10.462723177745671, 11.336370929336967});
        coordinates.add(new Double[]{10.0, 11.414213562373096});
        coordinates.add(new Double[]{9.604846812909845, 11.35788584156861});

        Area area = new Area("test", Area.AreaType.CIRCLE, positions);
        area.setId("1");
        PositionCosmos positionCosmos = new PositionCosmos("1", "Polygon", coordinates);

        assertEquals(positionCosmos, CosmosConverter.convertCircleAreaToPositionCosmos(area));
    }

    @Test
    void convertPointToPositionCosmos() {
        List<Double[]> coordinates = new ArrayList<>();

        double longitude = 10;
        double latitude = 9;

        coordinates.add(new Double[]{Double.valueOf(10), Double.valueOf(9)});
        PositionCosmos positionCosmos = new PositionCosmos(null, "Point", coordinates);

        assertEquals(positionCosmos, CosmosConverter.convertPointToPositionCosmos(longitude, latitude));
    }
}