package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class TimeIntervalConverterTest {
    @Test
    @DisplayName("Test conversion from model to domain class")
    void testConvertModelToDomain() {
        LocalTime time1 = LocalTime.of(8, 0);
        LocalTime time2 = LocalTime.of(17, 0);

        List<TimeInterval> timeIntervals = new ArrayList<>();
        timeIntervals.add(new TimeInterval(time1.toSecondOfDay(), time1.plusHours(1).toSecondOfDay()));
        timeIntervals.add(new TimeInterval(time2.toSecondOfDay(), time2.plusHours(2).toSecondOfDay()));

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval> timeIntervalsDomain = TimeIntervalConverter.modelToDomain
                .apply(timeIntervals);

        Assert.assertNotNull(timeIntervalsDomain);
        Assert.assertEquals(timeIntervals.size(), timeIntervalsDomain.size());
    }

    @Test
    @DisplayName("Test conversion from model to domain class with null model")
    void testConvertModelToDomainNull() {
        Assert.assertNull(TimeIntervalConverter.modelToDomain.apply(null));
    }

    @Test
    @DisplayName("Test conversion from domain to model")
    void testConvertDomainToModel() {
        LocalTime time1 = LocalTime.of(8, 0);
        LocalTime time2 = LocalTime.of(17, 0);

        List<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval> timeIntervals = new ArrayList<>();
        timeIntervals.add(new cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval(time1, time1.plusHours(1)));
        timeIntervals.add(new cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval(time2, time2.plusHours(2)));

        List<TimeInterval> timeIntervalsModel = TimeIntervalConverter.domainToModel.apply(timeIntervals);

        Assert.assertNotNull(timeIntervalsModel);
        Assert.assertEquals(timeIntervals.size(), timeIntervalsModel.size());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null domain")
    void testConvertDomainToModelNull() {
        Assert.assertNull(TimeIntervalConverter.domainToModel.apply(null));
    }
}
