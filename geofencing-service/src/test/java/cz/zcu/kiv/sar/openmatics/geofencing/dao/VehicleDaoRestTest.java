package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.openmatics.cloud.api.asset.management.IVehicleService;
import com.openmatics.cloud.lib.servicemanagement.util.IServiceInformationHolder;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VehicleDaoRestTest {

    private com.openmatics.cloud.core.api.Vehicle item;

    private com.openmatics.cloud.core.api.Vehicle item2;

    private VehicleDao vehicleDao;

    private IVehicleService vehicleService;

    private IServiceInformationHolder serviceInformationHolder;

    @BeforeEach
    void setUp(@Mock IVehicleService vehicleService, @Mock IServiceInformationHolder serviceInformationHolder) {
        this.vehicleService = vehicleService;
        this.vehicleDao = new VehicleDaoRest(vehicleService, serviceInformationHolder);
        this.serviceInformationHolder = serviceInformationHolder;

        item = new com.openmatics.cloud.core.api.Vehicle();
        item.setId("veh 1");
        item.setName("Vehicle 1");

        item2 = new com.openmatics.cloud.core.api.Vehicle();
        item2.setId("veh 2");
        item2.setName("Vehicle 2");
    }

    @Test
    void testFindAll() {
        List<com.openmatics.cloud.core.api.Vehicle> vehicles = new ArrayList<>();
        vehicles.add(item);
        vehicles.add(item2);

        List<Vehicle> list = new ArrayList<>();
        list.add(new Vehicle(item.getId(), item.getName()));
        list.add(new Vehicle(item2.getId(), item2.getName()));

        String clientId = "c1";

        when(serviceInformationHolder.getServiceClientId()).thenReturn(clientId);

        when(vehicleService.getVehiclesByClientId(clientId)).thenReturn(vehicles);
        assertEquals(list, vehicleDao.findAll());
    }
}