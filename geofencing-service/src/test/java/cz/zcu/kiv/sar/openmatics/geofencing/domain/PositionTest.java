package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link Position} entity.
 * <p>
 * Date: 23.11.17
 *
 * @author Marek Zimmermann
 */
public class PositionTest {
    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Create position")
    void testCreatePosition() {
        Position position = new Position(49.75000, 13.36670, 0);

        entityManager.persist(position);

        Position dbPosition = entityManager.createQuery("select p from Position p", Position.class).getSingleResult();

        assertEquals(position, dbPosition);
    }

    @Test
    @DisplayName("Test out of range latitude as negative value")
    void testOutOfRangeNegativeLatitude() {
        assertThrows(IllegalArgumentException.class,
                () -> new Position(
                        -200,
                        0,
                        0
                )
        );
    }

    @Test
    @DisplayName("Test out of range latitude as positive value")
    void testOutOfRangePositiveLatitude() {
        assertThrows(IllegalArgumentException.class,
                () -> new Position(
                        200,
                        0,
                        0
                )
        );
    }

    @Test
    @DisplayName("Test out of range longitude as negative value")
    void testOutOfRangeNegativeLongitude() {
        assertThrows(IllegalArgumentException.class,
                () -> new Position(
                        0,
                        -200,
                        0
                )
        );
    }

    @Test
    @DisplayName("Test out of range longitude as positive value")
    void testOutOfRangePositiveLongitude() {
        assertThrows(IllegalArgumentException.class,
                () -> new Position(
                        0,
                        200,
                        0
                )
        );
    }
}
