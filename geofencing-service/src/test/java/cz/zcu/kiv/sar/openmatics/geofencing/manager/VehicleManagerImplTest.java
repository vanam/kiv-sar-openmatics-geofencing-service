package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.VehicleDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.VehicleDaoRest;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VehicleManagerImplTest {

    private Vehicle item;

    private Vehicle item2;

    private VehicleManager vehicleManager;

    private VehicleDao vehicleDao;

    @BeforeEach
    void setUp(@Mock VehicleDaoRest vehicleDao, @Mock Vehicle item, @Mock Vehicle item2) {
        this.item = item;
        this.item2 = item2;

        this.vehicleDao = vehicleDao;
        this.vehicleManager = new VehicleManagerImpl(vehicleDao);
    }

    @Test
    void findAll() {
        List<Vehicle> list = new ArrayList<>();
        list.add(item);
        list.add(item2);

        when(vehicleDao.findAll()).thenReturn(list);
        assertEquals(list, vehicleManager.findAll());
    }

}
