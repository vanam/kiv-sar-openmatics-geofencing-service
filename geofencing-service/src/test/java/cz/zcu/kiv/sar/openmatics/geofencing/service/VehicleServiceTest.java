package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IVehicleService;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.VehicleManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VehicleServiceTest {

    private VehicleManager vehicleManager;

    private IVehicleService vehicleService;

    @BeforeEach
    void setUp(@Mock VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;

        vehicleService = new VehicleService(vehicleManager);
    }

    @Test
    @DisplayName("Get all vehicles")
    void testGetAreas() {
        Vehicle vehicle = new Vehicle();
        vehicle.setId("v1");
        vehicle.setName("Skoda 105");

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId("v2");
        vehicle2.setName("Skoda 120");


        List<Vehicle> list = new ArrayList<>();
        list.add(vehicle);
        list.add(vehicle2);

        when(vehicleManager.findAll()).thenReturn(list);

        assertEquals(list, vehicleService.getVehicles());
    }

}