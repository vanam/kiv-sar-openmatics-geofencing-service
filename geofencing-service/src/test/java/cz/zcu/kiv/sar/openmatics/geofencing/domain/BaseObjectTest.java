package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.Instant;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class BaseObjectTest {
    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Test new base object properties")
    void testNew() {
        BaseObject baseObject = new BaseObject();

        assertTrue(baseObject.isNew());
        assertFalse(baseObject.isDeleted());
        assertNull(baseObject.getCreateDate());
        assertNull(baseObject.getUpdateDate());
        assertNull(baseObject.getDeleteDate());
    }

    @Test
    @DisplayName("Test base object when persisted as some object.")
    void testPersisted() {
        BaseObject baseObject = new Area("i don't care", Area.AreaType.RECTANGLE, Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1)));

        entityManager.persist(baseObject);

        BaseObject dbBaseObject = entityManager
                .createQuery("select a from Area a", Area.class)
                .getSingleResult();

        assertFalse(dbBaseObject.isNew());
        assertFalse(dbBaseObject.isDeleted());
        assertNotNull(dbBaseObject.getCreateDate());
        assertNotNull(dbBaseObject.getUpdateDate());
        assertNull(dbBaseObject.getDeleteDate());
    }

    @Test
    @DisplayName("Test (soft-)deleted base object.")
    void testDeleted() {
        BaseObject baseObject = new Area("i don't care", Area.AreaType.RECTANGLE, Arrays.asList(new Position(0, 0, 0), new Position(0, 0, 1)));
        baseObject.setDeleteDate(Instant.now());// soft-delete object right away
        entityManager.persist(baseObject);

        BaseObject dbBaseObject = entityManager
                .createQuery("select a from Area a", Area.class)
                .getSingleResult();

        assertFalse(dbBaseObject.isNew());
        assertTrue(dbBaseObject.isDeleted());
        assertNotNull(dbBaseObject.getCreateDate());
        assertNotNull(dbBaseObject.getUpdateDate());
        assertNotNull(dbBaseObject.getDeleteDate());
    }
}