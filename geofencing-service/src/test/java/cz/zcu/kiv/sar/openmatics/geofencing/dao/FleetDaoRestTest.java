package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import com.openmatics.cloud.api.asset.management.IVehicleService;
import com.openmatics.cloud.core.api.Vehicle;
import com.openmatics.cloud.fleetmanagement.api.IFleetService;
import com.openmatics.cloud.fleetmanagement.model.FleetDetail;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FleetDaoRestTest {

    private FleetDetail item;

    private FleetDetail item2;

    private Vehicle v1;

    private Vehicle v2;

    private Vehicle v3;

    private List<cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle> vehicleList;

    private List<cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle> vehicleList2;

    private IFleetService fleetService;

    private IVehicleService vehicleService;

    private FleetDao fleetDao;

    @BeforeEach
    void setUp(@Mock IFleetService fleetService, @Mock IVehicleService vehicleService) {
        this.fleetService = fleetService;
        this.vehicleService = vehicleService;

        item = new FleetDetail();
        item2 = new FleetDetail();

        v1 = new Vehicle();
        v1.setId("veh 1");

        v2 = new Vehicle();
        v2.setId("veh 2");

        v3 = new Vehicle();
        v3.setId("veh 3");

        vehicleList = new ArrayList<>();
        vehicleList.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle(v1.getId(), v1.getName()));
        vehicleList.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle(v2.getId(), v2.getName()));

        vehicleList2 = new ArrayList<>();
        vehicleList2.add(new cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle(v3.getId(), v3.getName()));

        this.fleetDao = new FleetDaoRest(fleetService, vehicleService);
    }

    @Test
    @DisplayName("Find all fleets")
    void testFindAll() {
        List<String> vehicleID1 = new ArrayList<>();
        vehicleID1.add(v1.getId());
        vehicleID1.add(v2.getId());

        List<String> vehicleID2 = new ArrayList<>();
        vehicleID2.add(v3.getId());

        item.setId(1L);
        item.setName("fleet 1");
        item.setAssetsList(vehicleID1);

        item2.setId(2L);
        item2.setName("fleet 2");
        item2.setAssetsList(vehicleID2);

        List<FleetDetail> fleets = new ArrayList<>();
        fleets.add(item);
        fleets.add(item2);

        List<Fleet> list = new ArrayList<>();
        list.add(new Fleet(String.valueOf(item.getId()), item.getName(), vehicleList));
        list.add(new Fleet(String.valueOf(item2.getId()), item2.getName(), vehicleList2));

        when(vehicleService.get(v1.getId())).thenReturn(v1);
        when(vehicleService.get(v2.getId())).thenReturn(v2);
        when(vehicleService.get(v3.getId())).thenReturn(v3);
        when(fleetService.getFleets(true, null)).thenReturn(fleets);

        assertEquals(list, fleetDao.findAll());
    }

    @Test
    @DisplayName("Find fleets by by vehicle ID")
    void testFindFleetByVehicleId() {
        String id = v1.getId();


        List<String> vehicleID1 = new ArrayList<>();
        vehicleID1.add(v1.getId());
        vehicleID1.add(v2.getId());

        List<String> vehicleID2 = new ArrayList<>();
        vehicleID2.add(v3.getId());

        item.setId(1L);
        item.setName("fleet 1");
        item.setAssetsList(vehicleID1);

        item2.setId(2L);
        item2.setName("fleet 2");
        item2.setAssetsList(vehicleID2);

        List<FleetDetail> fleets = new ArrayList<>();
        fleets.add(item);
        fleets.add(item2);

        when(fleetService.getFleets(true, null)).thenReturn(fleets);

        List<Fleet> list = new ArrayList<>();
        list.add(new Fleet(String.valueOf(1L), "fleet 1", null));

        assertEquals(list, fleetDao.findFleetByVehicleId(id));
    }

    @Test
    @DisplayName("Find fleets by by vehicle ID in fleet with null vehicle list")
    void testFindFleetByVehicleId2() {
        String id = v1.getId();


        List<String> vehicleID1 = new ArrayList<>();
        vehicleID1.add(v1.getId());
        vehicleID1.add(v2.getId());

        item.setId(1L);
        item.setName("fleet 1");
        item.setAssetsList(vehicleID1);

        item2.setId(2L);
        item2.setName("fleet 2");
        item2.setAssetsList(null);

        List<FleetDetail> fleets = new ArrayList<>();
        fleets.add(item);
        fleets.add(item2);

        when(fleetService.getFleets(true, null)).thenReturn(fleets);

        List<Fleet> list = new ArrayList<>();
        list.add(new Fleet(String.valueOf(1L), "fleet 1", null));

        assertEquals(list, fleetDao.findFleetByVehicleId(id));
    }
}
