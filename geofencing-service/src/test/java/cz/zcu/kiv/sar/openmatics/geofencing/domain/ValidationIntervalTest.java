package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationIntervalTest {

    @Test
    @DisplayName("Create validation interval")
    void testCreateValidationInterval() {
        ValidationInterval interval = new ValidationInterval(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2020, 12, 31)
        );

        assertEquals(LocalDate.of(2001, 1, 1), interval.getDateFrom());
        assertEquals(LocalDate.of(2020, 12, 31), interval.getDateTo());
    }

    @Test
    @DisplayName("Create validation interval with null date from")
    void testNullDateFrom() {
        assertThrows(IllegalArgumentException.class,
                () -> new ValidationInterval(
                        null,
                        LocalDate.of(2020, 12, 31)
                )
        );
    }

    @Test
    @DisplayName("Create validation interval with null date to")
    void testNullDateTo() {
        assertThrows(IllegalArgumentException.class,
                () -> new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        null
                )
        );
    }
}