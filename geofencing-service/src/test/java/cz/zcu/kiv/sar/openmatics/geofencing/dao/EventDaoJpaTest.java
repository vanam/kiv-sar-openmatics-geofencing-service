package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDaoConfig.class)
@Transactional
class EventDaoJpaTest extends GenericDaoJpaTest {

    private AreaDaoJpa areaDao;

    private RuleDaoJpa ruleDao;

    @BeforeEach
    void setUp(@Autowired EventDaoJpa eventDao, @Autowired AreaDaoJpa areaDao, @Autowired RuleDaoJpa ruleDao) {
        this.areaDao = areaDao;
        this.ruleDao = ruleDao;
        this.dao = eventDao;

        item = new Event(
                Event.EventType.VIOLATION,
                getTestRule(),
                "vehicle id",
                LocalDateTime.now()
        );

        item2 = new Event(
                Event.EventType.CORRECTION,
                getTestRule(),
                "vehicle id 2",
                LocalDateTime.now()
        );
    }

    private Rule getTestRule() {
        Area area = new Area
                (
                        "area name",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE);

        areaDao.save(area);
        ruleDao.save(rule);

        return rule;
    }

}