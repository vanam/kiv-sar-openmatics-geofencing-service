package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.ValidationInterval;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Validity;
import cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ValidityConverterTest
{
    @Test
    @DisplayName("Test conversion from model to domain class")
    void testConvertModelToDomain()
    {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();

        Validity domainValidity = ValidityConverter.modelToDomain.apply(modelValidity);

        Assertions.assertNotNull(domainValidity);
        Assertions.assertEquals(modelValidity.getValidationInterval().getDateFrom(),
                domainValidity.getValidationInterval().getDateFrom().toEpochDay());
        Assertions.assertEquals(modelValidity.getValidationInterval().getDateTo(),
                domainValidity.getValidationInterval().getDateTo().toEpochDay());
    }

    @Test
    @DisplayName("Test conversion from model with null validation interval to domain class")
    void testConvertModelNullValidationIntervalToDomain()
    {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();

        modelValidity.setValidationInterval(null);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ValidityConverter.modelToDomain.apply(modelValidity));
    }

    @Test
    @DisplayName("Test conversion from model with null time intervals list to domain class")
    void testConvertModelNullTimeIntervalsToDomain()
    {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();

        modelValidity.setTimeIntervals(null);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ValidityConverter.modelToDomain.apply(modelValidity));
    }

    @Test
    @DisplayName("Test conversion from model with bad count of time intervals in the list to domain class")
    void testConvertModelBadCountTimeIntervalsToDomain()
    {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = getTestModelValidity();

        List<TimeInterval>[] timeIntervals = new List[6];
        for (int i = 0; i < timeIntervals.length; i++)
        {
            timeIntervals[i] = new ArrayList<>();
        }

        modelValidity.setTimeIntervals(timeIntervals);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ValidityConverter.modelToDomain.apply(modelValidity));
    }

    @Test
    @DisplayName("Test conversion from domain to model class")
    void testConvertDomainToModel()
    {
        Validity domainValidity = getTestDomainValidity();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = ValidityConverter.domainToModel
                .apply(domainValidity);

        Assertions.assertNotNull(modelValidity);
        Assertions.assertEquals(domainValidity.getValidationInterval().getDateFrom().toEpochDay(),
                modelValidity.getValidationInterval().getDateFrom());
        Assertions.assertEquals(domainValidity.getValidationInterval().getDateTo().toEpochDay(),
                modelValidity.getValidationInterval().getDateTo());
    }

    @Test
    @DisplayName("Test conversion from domain with null validation interval to model class")
    void testConvertDomainWithNullValidationIntervalToModel()
    {
        Validity domainValidity = getTestDomainValidity();
        domainValidity.setValidationInterval(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = ValidityConverter.domainToModel
                .apply(domainValidity);

        Assertions.assertNotNull(modelValidity);
        Assertions.assertNull(modelValidity.getValidationInterval());
    }

    @Test
    @DisplayName("Test conversion from domain with null to date in validation interval to model class")
    void testConvertDomainWithNullFromDateValidationIntervalToModel()
    {
        Validity domainValidity = getTestDomainValidity();
        domainValidity.getValidationInterval().setDateTo(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Validity modelValidity = ValidityConverter.domainToModel
                .apply(domainValidity);

        Assertions.assertNotNull(modelValidity);
        Assertions.assertNull(modelValidity.getValidationInterval());
    }

    private cz.zcu.kiv.sar.openmatics.geofencing.model.Validity getTestModelValidity()
    {
        LocalDate date1 = LocalDate.of(2001, 1, 1);
        LocalDate date2 = LocalDate.of(2020, 12, 31);

        List<TimeInterval>[] timeIntervals = new List[7];
        for (int i = 0; i < timeIntervals.length; i++)
        {
            timeIntervals[i] = new ArrayList<>();
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.model.Validity(
                new cz.zcu.kiv.sar.openmatics.geofencing.model.ValidationInterval(date1.toEpochDay(),
                        date2.toEpochDay()), timeIntervals);
    }

    private Validity getTestDomainValidity()
    {
        return new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections
                        .singletonList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval(LocalTime.of(1, 15),
                                LocalTime.of(10, 30))),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>(),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>(),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>(),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>(),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>(),
                new ArrayList<cz.zcu.kiv.sar.openmatics.geofencing.domain.TimeInterval>()
        );
    }
}
