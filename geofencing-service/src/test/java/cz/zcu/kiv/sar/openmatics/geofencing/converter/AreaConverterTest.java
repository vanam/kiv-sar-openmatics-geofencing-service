package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.configuration.ContextConfiguration;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Position;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class AreaConverterTest {
    @Test
    @DisplayName("Test conversion from model to domain class")
    void testConvertModelToDomain() {
        Instant testInstant = Instant.from(ZonedDateTime
                .of(LocalDateTime.of(2017, 1, 1, 8, 0, 0), ZoneId.of(ContextConfiguration.DEFAULT_TIME_ZONE)));
        Area area = new Area("test-id", "test name 1", Area.AreaType.RECTANGLE,
                Arrays.asList(new Position(12, 13), new Position(13, 14)), testInstant.toEpochMilli());

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area domainArea = AreaConverter.modelToDomain.apply(area);

        Assertions.assertNotNull(domainArea);
    }

    @Test
    @DisplayName("Test conversion of model area with null area type.")
    void testConvertModelToDomainNullAreaType() {
        Instant testInstant = Instant.from(ZonedDateTime
                .of(LocalDateTime.of(2017, 1, 1, 8, 0, 0), ZoneId.of(ContextConfiguration.DEFAULT_TIME_ZONE)));
        Area area = new Area("test-id", "test name", null,
                Arrays.asList(new Position(12, 13), new Position(13, 14)),
                testInstant.toEpochMilli());

        assertThrows(IllegalArgumentException.class, () -> AreaConverter.modelToDomain.apply(area));
    }

    @Test
    @DisplayName("Test conversion of model area with null list of positions.")
    void testConvertModelToDomainNullPositions() {
        Instant testInstant = Instant.from(ZonedDateTime
                .of(LocalDateTime.of(2017, 1, 1, 8, 0, 0), ZoneId.of(ContextConfiguration.DEFAULT_TIME_ZONE)));
        Area area = new Area("test-id", "test name", Area.AreaType.RECTANGLE,
                null, testInstant.toEpochMilli());

        assertThrows(IllegalArgumentException.class, () -> AreaConverter.modelToDomain.apply(area));
    }

    @Test
    @DisplayName("Test conversion from domain to model")
    void testConvertDomainToModel() {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(
                "Test domain area", cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE,
                Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(12, 13, 0),
                        new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(13, 14, 1)));
        area.setId("test-id");
        area.setCreateDate(Instant.now());

        Area modelArea = AreaConverter.domainToModel.apply(area);

        Assertions.assertNotNull(modelArea);
        Assertions.assertEquals("test-id", modelArea.getId());
    }

    @Test
    @DisplayName("Test successful conversion of domain area with out of long bounds (underflow) Instant.")
    void testConvertDomainToModelUnderflowCreateDate() {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(
                "Test domain area", cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE,
                Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(12, 13, 0),
                        new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(13, 14, 1)));
        area.setId("test-id");
        area.setCreateDate(Instant.MIN);

        Area modelArea = AreaConverter.domainToModel.apply(area);

        Assertions.assertNotNull(modelArea);
        Assertions.assertEquals(Long.MIN_VALUE, modelArea.getCreateDateMillis());
    }

    @Test
    @DisplayName("Test successful conversion of domain area with out of long bounds (overflow) Instant.")
    void testConvertDomainToModelOverflowCreateDate() {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(
                "Test domain area", cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE,
                Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(12, 13, 0),
                        new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(13, 14, 1)));
        area.setId("test-id");
        area.setCreateDate(Instant.MAX);

        Area modelArea = AreaConverter.domainToModel.apply(area);

        Assertions.assertNotNull(modelArea);
        Assertions.assertEquals(Long.MAX_VALUE, modelArea.getCreateDateMillis());
    }

    @Test
    @DisplayName("Test conversion of area with null area type.")
    void testConvertDomainToModelNullAreaType() {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(
                "Test domain area", cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE,
                Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(12, 13, 0),
                        new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(13, 14, 1)));
        area.setId("test-id");
        area.setCreateDate(LocalDateTime.of(2017, 12, 12, 8, 0, 0, 0)
                .atZone(ZoneId.of(ContextConfiguration.DEFAULT_TIME_ZONE_OFFSET)).toInstant());

        area.setType(null);

        Area modelArea = AreaConverter.domainToModel.apply(area);

        assertNotNull(modelArea);
        assertNull(modelArea.getType());
    }

    @Test
    @DisplayName("Test conversion of area with null list with positions.")
    void testConvertDomainToModelNullPositions() {
        cz.zcu.kiv.sar.openmatics.geofencing.domain.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.domain.Area(
                "Test domain area", cz.zcu.kiv.sar.openmatics.geofencing.domain.Area.AreaType.RECTANGLE,
                Arrays.asList(new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(12, 13, 0),
                        new cz.zcu.kiv.sar.openmatics.geofencing.domain.Position(13, 14, 1)));
        area.setId("test-id");
        area.setCreateDate(LocalDateTime.of(2017, 12, 12, 8, 0, 0, 0)
                .atZone(ZoneId.of(ContextConfiguration.DEFAULT_TIME_ZONE_OFFSET)).toInstant());

        area.setPositions(null);

        Area modelArea = AreaConverter.domainToModel.apply(area);

        assertNotNull(modelArea);
        assertNull(modelArea.getPositionList());
    }
}
