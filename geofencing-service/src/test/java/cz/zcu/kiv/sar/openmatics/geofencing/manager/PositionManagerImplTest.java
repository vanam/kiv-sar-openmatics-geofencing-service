package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.converter.CosmosConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.PositionDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.PositionDaoCosmos;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PositionManagerImplTest {

    private PositionDao positionDao;

    private PositionManager positionManager;

    @BeforeEach
    void setUp(@Mock PositionDaoCosmos positionDao) {
        this.positionDao = positionDao;
        this.positionManager = new PositionManagerImpl(positionDao);
    }

    @Test
    void getInsidePositionIds() {
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("2");

        when(positionDao.getInsidePositionIds(CosmosConverter.convertPointToPositionCosmos(10, 9))).thenReturn(ids);
        assertEquals(ids, positionManager.getInsidePositionIds(10, 9));
    }

    @Test
    void getOutsidePositionIds() {
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("2");

        when(positionDao.getOutsidePositionIds(CosmosConverter.convertPointToPositionCosmos(10, 9))).thenReturn(ids);
        assertEquals(ids, positionManager.getOutsidePositionIds(10, 9));
    }
}