package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleDaoJpa;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleViolationDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.RuleViolationDaoJpa;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RuleManagerImplTest extends GenericManagerImplTest<Rule> {

    private RuleManager ruleManager;
    private PositionManager positionManager;
    private RuleDao ruleDao;
    private RuleViolationDao ruleViolationDao;

    private Area area;
    private Rule rule;
    private Rule rule2;

    @BeforeEach
    void setUp(@Mock RuleDaoJpa ruleDao, @Mock RuleViolationDaoJpa ruleViolationDao, @Mock PositionManagerImpl positionManager, @Mock FleetManagerImpl fleetManager, @Mock Rule item, @Mock Rule item2) {
        this.item = item;
        this.item2 = item2;
        this.dao = ruleDao;
        manager = new RuleManagerImpl(ruleDao, ruleViolationDao, positionManager, fleetManager);
        ruleManager = (RuleManager) manager;
        this.positionManager = positionManager;
        this.ruleDao = ruleDao;
        this.ruleViolationDao = ruleViolationDao;

        area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );
        area.setId("a1");

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                new ArrayList<TimeInterval>() {{
                    add(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30)));
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        this.rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                new ArrayList<Area>() {{
                    add(area);
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE
        );

        this.rule2 = new Rule(
                "test rule 2",
                "desc",
                validity,
                new ArrayList<Area>() {{
                    add(area);
                }},
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE
        );

    }

    @Test
    void testMatchRulesInside() {
        List<Rule> rules = new ArrayList<>();
        rules.add(rule);
        rules.add(rule2);

        List<String> positions = new ArrayList<>();
        positions.add(area.getId());

        RuleViolation ruleViolation = new RuleViolation("1", rule2);

        List<RuleViolation> ruleViolations = new ArrayList<>();
        ruleViolations.add(ruleViolation);

        when(positionManager.getInsidePositionIds(2, 2)).thenReturn(positions);
        when(positionManager.getOutsidePositionIds(2, 2)).thenReturn(new ArrayList<>());
        when(ruleDao.findRulesByFleetId("f1")).thenReturn(null);
        when(ruleDao.findRulesByVehicleId("1")).thenReturn(rules);
        when(ruleViolationDao.save(ruleViolation)).thenReturn(ruleViolation);
        assertEquals(ruleViolations, ruleManager.matchRules("1", LocalDateTime.of(2017, 1, 2, 3, 0), 2, 2));
    }

    @Test
    void testMatchRulesOutside() {
        List<Rule> rules = new ArrayList<>();
        rules.add(rule);
        rules.add(rule2);

        List<String> positions = new ArrayList<>();
        positions.add(area.getId());

        RuleViolation ruleViolation = new RuleViolation("1", rule);

        List<RuleViolation> ruleViolations = new ArrayList<>();
        ruleViolations.add(ruleViolation);

        when(positionManager.getInsidePositionIds(3, 3)).thenReturn(new ArrayList<>());
        when(positionManager.getOutsidePositionIds(3, 3)).thenReturn(positions);
        when(ruleDao.findRulesByFleetId("f1")).thenReturn(null);
        when(ruleDao.findRulesByVehicleId("1")).thenReturn(rules);
        when(ruleViolationDao.save(ruleViolation)).thenReturn(ruleViolation);
        assertEquals(ruleViolations, ruleManager.matchRules("1", LocalDateTime.of(2017, 1, 2, 3, 0), 3, 3));
    }

    @Test
    void testMatchRulesOutsideOfTimeInterval() {
        List<Rule> rules = new ArrayList<>();
        rules.add(rule);
        rules.add(rule2);

        List<String> positions = new ArrayList<>();
        positions.add(area.getId());

        List<RuleViolation> ruleViolations = new ArrayList<>();

        when(positionManager.getInsidePositionIds(2, 2)).thenReturn(positions);
        when(positionManager.getOutsidePositionIds(2, 2)).thenReturn(new ArrayList<>());
        when(ruleDao.findRulesByFleetId("f1")).thenReturn(null);
        when(ruleDao.findRulesByVehicleId("1")).thenReturn(rules);
        assertEquals(ruleViolations, ruleManager.matchRules("1", LocalDateTime.of(2027, 1, 2, 3, 0), 2, 2));
    }
}