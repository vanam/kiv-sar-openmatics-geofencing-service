package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.ArrayList;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestDaoConfig.class)
@Transactional
class AreaDaoJpaTest extends GenericDaoJpaTest {

    @BeforeEach
    void setUp(@Autowired AreaDaoJpa dao) {
        this.dao = dao;
        item = new Area(
                "rectangle",
                Area.AreaType.RECTANGLE,
                // Modifiable list here
                new ArrayList<Position>() {{
                    add(new Position(0, 0, 0));
                    add(new Position(0, 0, 1));
                }}
        );

        item2 = new Area(
                "circle",
                Area.AreaType.CIRCLE,
                // Modifiable list here
                new ArrayList<Position>() {{
                    add(new Position(0, 0, 0));
                    add(new Position(0, 0, 1));
                }}
        );
    }

}
