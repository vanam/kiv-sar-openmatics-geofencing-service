package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IRuleService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.RuleConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.AreaManager;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.RuleManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import javax.ws.rs.ClientErrorException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RuleServiceTest {

    private AreaManager areaManager;

    private RuleManager ruleManager;

    private IRuleService ruleService;

    @BeforeEach
    void setUp(@Mock AreaManager areaManager, @Mock RuleManager ruleManager) {
        this.areaManager = areaManager;
        this.ruleManager = ruleManager;

        ruleService = new RuleService(areaManager, ruleManager);
    }

    @Test
    @DisplayName("Get all rules")
    void testGetRules() {
        Rule rule = getTestRule(1);
        rule.setId("rule id 1");
        rule.setCreateDate(Instant.now());

        Rule rule2 = getTestRule(2);
        rule2.setId("rule id 2");
        rule2.setCreateDate(Instant.now());

        List<Rule> list = new ArrayList<>();
        list.add(rule);
        list.add(rule2);

        when(ruleManager.findAll()).thenReturn(list);
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Rule> expectedList = list.stream()
                .map(RuleConverter::domainToModel)
                .collect(Collectors.toList());
        assertEquals(expectedList, ruleService.getRules());
    }

    @Test
    @DisplayName("Add new rule")
    void testAddRule() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
        area.setId("area 1");

        Area foundArea = new Area(
                "area name 1",
                Area.AreaType.RECTANGLE,
                Arrays.asList(
                        new Position(1, 2, 0),
                        new Position(2, 3, 1)
                )
        );
        foundArea.setId("area 1");
        foundArea.setCreateDate(Instant.now());

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
        modelRule.setName("rule");
        modelRule.setDescription("description");
        modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
        modelRule.setAreaList(Collections.singletonList(area));
        modelRule.setValidity(getModelValidity());
        modelRule.setFleetIdList(Collections.emptyList());
        modelRule.setVehicleIdList(Collections.emptyList());

        when(areaManager.findOne("area 1")).thenReturn(foundArea);

        Rule expectedRule = RuleConverter.modelToDomain(modelRule, null, areaManager);
        expectedRule.setId("rule 1");
        expectedRule.setCreateDate(Instant.now());

        when(ruleManager.save(any(Rule.class))).thenReturn(expectedRule);
        assertEquals(RuleConverter.domainToModel(expectedRule), ruleService.addRule(modelRule));
    }

    @Test
    @DisplayName("Add rule with invalid name")
    void testAddRule2() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid description")
    void testAddRule3() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid type")
    void testAddRule4() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setDescription("description");
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid area list")
    void testAddRule5() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid validity")
    void testAddRule6() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid fleet ID list")
    void testAddRule7() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Add rule with invalid vehicle ID list")
    void testAddRule8() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setName("rule");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);

                    ruleService.addRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule")
    void testUpdateRule() {
        cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
        area.setId("area 1");

        Area foundArea = new Area(
                "area name 1",
                Area.AreaType.RECTANGLE,
                Arrays.asList(
                        new Position(1, 2, 0),
                        new Position(2, 3, 1)
                )
        );
        foundArea.setId("area 1");
        foundArea.setCreateDate(Instant.now());

        cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
        modelRule.setId("rule 1");
        modelRule.setName("rule name");
        modelRule.setDescription("description");
        modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
        modelRule.setAreaList(Collections.singletonList(area));
        modelRule.setValidity(getModelValidity());
        modelRule.setFleetIdList(Collections.emptyList());
        modelRule.setVehicleIdList(Collections.emptyList());

        when(areaManager.findOne("area 1")).thenReturn(foundArea);

        Rule oldRule = getTestRule(3);
        oldRule.setId("rule 1");
        oldRule.setCreateDate(Instant.MIN);

        Rule expectedRule = RuleConverter.modelToDomain(modelRule, oldRule, areaManager);
        expectedRule.setId("rule 1");
        expectedRule.setCreateDate(Instant.MIN);
        expectedRule.setUpdateDate(Instant.now());


        when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
        when(ruleManager.save(any(Rule.class))).thenReturn(expectedRule);
        assertEquals(RuleConverter.domainToModel(expectedRule), ruleService.updateRule(modelRule));
    }

    @Test
    @DisplayName("Update rule with invalid name")
    void testUpdateRule2() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid description")
    void testUpdateRule3() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid type")
    void testUpdateRule4() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setDescription("description");
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid area list")
    void testUpdateRule5() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid validity")
    void testUpdateRule6() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setFleetIdList(Collections.emptyList());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid fleet ID list")
    void testUpdateRule7() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setVehicleIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Update rule with invalid vehicle ID list")
    void testUpdateRule8() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    cz.zcu.kiv.sar.openmatics.geofencing.model.Area area = new cz.zcu.kiv.sar.openmatics.geofencing.model.Area();
                    area.setId("area 1");

                    Area foundArea = new Area(
                            "area name 1",
                            Area.AreaType.RECTANGLE,
                            Arrays.asList(
                                    new Position(1, 2, 0),
                                    new Position(2, 3, 1)
                            )
                    );
                    foundArea.setId("area 1");
                    foundArea.setCreateDate(Instant.now());

                    cz.zcu.kiv.sar.openmatics.geofencing.model.Rule modelRule = new cz.zcu.kiv.sar.openmatics.geofencing.model.Rule();
                    modelRule.setId("rule 1");
                    modelRule.setName("rule name");
                    modelRule.setDescription("description");
                    modelRule.setType(cz.zcu.kiv.sar.openmatics.geofencing.model.Rule.Type.OUTSIDE);
                    modelRule.setAreaList(Collections.singletonList(area));
                    modelRule.setValidity(getModelValidity());
                    modelRule.setFleetIdList(Collections.emptyList());

                    Rule oldRule = getTestRule(3);
                    oldRule.setId("rule 1");
                    oldRule.setCreateDate(Instant.MIN);

                    when(areaManager.findOne("area 1")).thenReturn(foundArea);
                    when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
                    ruleService.updateRule(modelRule);
                });
    }

    @Test
    @DisplayName("Delete non-existing rule")
    void testDeleteRule() {
        assertThrows(ClientErrorException.class,
                () -> ruleService.deleteRule("rule 1"));
    }

    @Test
    @DisplayName("Duplicate existing rule")
    void testDuplicateRule() {
        Rule oldRule = getTestRule(3);
        oldRule.setId("rule 1");
        oldRule.setCreateDate(Instant.MIN);

        Rule expectedRule = new Rule(
                oldRule.getName(),
                oldRule.getDescription(),
                oldRule.getValidity(),
                oldRule.getAreas(),
                oldRule.getVehicles(),
                oldRule.getFleets(),
                oldRule.getType()
        );
        expectedRule.setId("rule other than 1");
        expectedRule.setCreateDate(Instant.MIN);
        expectedRule.setUpdateDate(Instant.now());


        when(ruleManager.findOne("rule 1")).thenReturn(oldRule);
        when(ruleManager.save(any(Rule.class))).thenReturn(expectedRule);
        assertEquals(RuleConverter.domainToModel(expectedRule), ruleService.duplicateRule(oldRule.getId()));
    }

    @Test
    @DisplayName("Duplicate non-existing rule")
    void testDuplicateRule2() {
        assertThrows(ClientErrorException.class,
                () -> ruleService.duplicateRule("rule 1"));
    }


    private cz.zcu.kiv.sar.openmatics.geofencing.model.Validity getModelValidity() {
        LocalDate date1 = LocalDate.of(2001, 1, 1);
        LocalDate date2 = LocalDate.of(2021, 12, 31);

        List<cz.zcu.kiv.sar.openmatics.geofencing.model.TimeInterval>[] timeIntervals = new List[7];
        for (int i = 0; i < timeIntervals.length; i++) {
            timeIntervals[i] = new ArrayList<>();
        }

        return new cz.zcu.kiv.sar.openmatics.geofencing.model.Validity(
                new cz.zcu.kiv.sar.openmatics.geofencing.model.ValidationInterval(date1.toEpochDay(),
                        date2.toEpochDay()), timeIntervals);
    }

    private Rule getTestRule(int i) {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );
        area.setId("area 1");
        area.setCreateDate(Instant.now());

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        return new Rule(
                "test rule " + i,
                "desc " + i * i,
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                i % 2 == 0 ? Rule.Type.INSIDE : Rule.Type.OUTSIDE);
    }
}