package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IEventService;
import cz.zcu.kiv.sar.openmatics.geofencing.converter.EventConverter;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.EventManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

    private EventManager eventManager;

    private IEventService eventService;

    @BeforeEach
    void setUp(@Mock EventManager eventManager) {
        this.eventManager = eventManager;

        eventService = new EventService(eventManager);
    }

    @Test
    @DisplayName("Get all areas")
    void testGetAreas() {
        List<Event> list = new ArrayList<>();
        list.add(getTestEvent(0));
        list.add(getTestEvent(1));

        when(eventManager.findAll()).thenReturn(list);
        List<cz.zcu.kiv.sar.openmatics.geofencing.model.Event> expectedList = list.stream()
                .map(EventConverter.domainToModel)
                .collect(Collectors.toList());
        assertEquals(expectedList, eventService.getEvents());
    }

    private Event getTestEvent(int i) {
        return new Event(
                i % 2 == 0 ? Event.EventType.VIOLATION : Event.EventType.CORRECTION,
                getTestRule(i),
                "veh123",
                LocalDateTime.now()
        );
    }

    private Rule getTestRule(int i) {
        Area area = new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 4, 1)
                                )
                );
        area.setId("area 1");
        area.setCreateDate(Instant.now());

        Validity validity = new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );

        return new Rule(
                "test rule " + i,
                "desc " + i * i,
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                i % 2 == 0 ? Rule.Type.INSIDE : Rule.Type.OUTSIDE);
    }
}