package cz.zcu.kiv.sar.openmatics.geofencing.service;

import cz.zcu.kiv.sar.openmatics.geofencing.api.IFleetService;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.manager.FleetManager;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FleetServiceTest {

    private FleetManager fleetManager;

    private IFleetService fleetService;

    @BeforeEach
    void setUp(@Mock FleetManager fleetManager) {
        this.fleetManager = fleetManager;

        fleetService = new FleetService(fleetManager);
    }

    @Test
    @DisplayName("Get all fleets")
    void testGetAreas() {
        Vehicle vehicle = new Vehicle();
        vehicle.setId("v1");
        vehicle.setName("Skoda 105");

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId("v2");
        vehicle2.setName("Skoda 120");

        Fleet fleet = new Fleet();
        fleet.setId("f1");
        fleet.setName("Fleet 1");
        fleet.setVehicles(Lists.emptyList());

        Fleet fleet2 = new Fleet();
        fleet2.setId("f2");
        fleet2.setName("Fleet 2");
        fleet2.setVehicles(Arrays.asList(vehicle, vehicle2));

        List<Fleet> list = new ArrayList<>();
        list.add(fleet);
        list.add(fleet2);

        when(fleetManager.findAll()).thenReturn(list);

        assertEquals(list, fleetService.getFleets());
    }

}