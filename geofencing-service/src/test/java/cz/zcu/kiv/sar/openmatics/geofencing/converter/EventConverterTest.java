package cz.zcu.kiv.sar.openmatics.geofencing.converter;

import cz.zcu.kiv.sar.openmatics.geofencing.domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class EventConverterTest {
    @Test
    @DisplayName("Test conversion from domain to model")
    void testConvertDomainToModel() {
        Event event = getTestEvent();

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event modelEvent = EventConverter.domainToModel.apply(event);

        Assertions.assertNotNull(modelEvent);
        Assertions.assertNotNull(modelEvent.getRule());
    }

    @Test
    @DisplayName("Test conversion from domain to model with null domain")
    void testConvertDomainToModelNull() {
        Assertions.assertNull(EventConverter.domainToModel.apply(null));
    }

    @Test
    @DisplayName("Test conversion from domain object with null type to model")
    void testConvertDomainToModelNullType() {
        Event event = getTestEvent();
        event.setType(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event modelEvent = EventConverter.domainToModel.apply(event);

        Assertions.assertNotNull(modelEvent);
        Assertions.assertNull(modelEvent.getName());
    }

    @Test
    @DisplayName("Test conversion from domain object with null rule to model")
    void testConvertDomainToModelNullRule() {
        Event event = getTestEvent();
        event.setRule(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event modelEvent = EventConverter.domainToModel.apply(event);

        Assertions.assertNotNull(modelEvent);
        Assertions.assertNull(modelEvent.getRule());
    }

    @Test
    @DisplayName("Test conversion from domain object with null vehicle ID to model")
    void testConvertDomainToModelNullVehicleId() {
        Event event = getTestEvent();
        event.setVehicleId(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event modelEvent = EventConverter.domainToModel.apply(event);

        Assertions.assertNotNull(modelEvent);
        Assertions.assertNull(modelEvent.getVehicleId());
    }

    @Test
    @DisplayName("Test conversion from domain object with null event time to model")
    void testConvertDomainToModelNullEventTime() {
        Event event = getTestEvent();
        event.setEventTime(null);

        cz.zcu.kiv.sar.openmatics.geofencing.model.Event modelEvent = EventConverter.domainToModel.apply(event);

        Assertions.assertNotNull(modelEvent);
        Assertions.assertEquals(Long.MIN_VALUE, modelEvent.getEventTime());
    }

    private Event getTestEvent() {
        Area area = new Area(
                "test area",
                Area.AreaType.RECTANGLE,
                new ArrayList<Position>(Arrays.asList(
                        new Position(10, 50, 0),
                        new Position(11, 51, 1)
                ))
        );
        area.setId("test-area");
        area.setCreateDate(Instant.now());

        VehicleId vehicleId = new VehicleId("test-vehicle-id");
        ValidationInterval validationInterval = new ValidationInterval(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2020, 12, 31)
        );
        Validity validity = new Validity(
                validationInterval,
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList()
        );
        Rule rule = new Rule(
                "Test rule",
                "Description for test rule",
                validity,
                new ArrayList<Area>(Collections.singletonList(area)),
                Collections.singletonList(vehicleId),
                new ArrayList<>(),
                Rule.Type.INSIDE);

        cz.zcu.kiv.sar.openmatics.geofencing.domain.Event.EventType eventType = cz.zcu.kiv.sar.openmatics.geofencing.domain.Event.EventType.VIOLATION;

        return new Event(
                eventType,
                rule,
                vehicleId.getVehicleId(),
                LocalDateTime.of(2017, 12, 1, 12, 0)
        );
    }
}
