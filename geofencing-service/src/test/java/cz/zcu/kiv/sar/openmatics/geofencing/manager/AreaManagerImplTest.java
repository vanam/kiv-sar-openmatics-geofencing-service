package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.AreaDaoJpa;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.Area;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AreaManagerImplTest extends GenericManagerImplTest<Area> {

    private AreaManager areaManager;

    @BeforeEach
    void setUp(@Mock AreaDaoJpa dao, @Mock Area item, @Mock Area item2) {
        this.item = item;
        this.item2 = item2;
        this.dao = dao;
        manager = new AreaManagerImpl(dao);
        areaManager = (AreaManager) manager;
    }

    @Test
    void testCount2() {
        when(dao.getCount()).thenReturn(3L);
        assertEquals(3L, areaManager.getCount());
    }

}