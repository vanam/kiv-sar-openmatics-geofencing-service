package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link Validity} entity.
 * <p>
 * Date: 23.11.17
 *
 * @author Marek Zimmermann
 */
public class ValidityTest {
    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Create validity")
    void testCreateValidity() {
        Validity validity = new Validity(new ValidationInterval(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2020, 12, 31)
        ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>());

        entityManager.persist(validity);

        Validity dbValidity = entityManager.createQuery("select v from Validity v", Validity.class).getSingleResult();

        assertEquals(validity, dbValidity);
    }

    @Test
    @DisplayName("Test validation with no time intervals")
    void testNoTimeInterval() {
        Validity validity = new Validity(new ValidationInterval(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2020, 12, 31)
        ),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>()
        );

        entityManager.persist(validity);

        Validity dbValidity = entityManager.createQuery("select v from Validity v", Validity.class).getSingleResult();

        assertEquals(validity, dbValidity);
    }

    @Test
    @DisplayName("Test null validation interval")
    void testNullValidationInterval() {
        assertThrows(IllegalArgumentException.class,
                () -> new Validity(null,
                        Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>())
        );
    }

    @Test
    @DisplayName("Test null 'from' attribute in validation interval")
    void testNullFromAttValidationInterval() {
        assertThrows(IllegalArgumentException.class,
                () -> new Validity(new ValidationInterval(
                        null,
                        LocalDate.of(2020, 12, 31)
                ),
                        Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>())
        );
    }

    @Test
    @DisplayName("Test null 'to' attribute in validation interval")
    void testNullToAttValidationInterval() {
        assertThrows(IllegalArgumentException.class,
                () -> new Validity(new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        null
                ),
                        Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>(),
                        new ArrayList<TimeInterval>())
        );
    }
}
