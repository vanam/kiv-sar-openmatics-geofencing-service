package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.GenericDaoJpa;
import cz.zcu.kiv.sar.openmatics.geofencing.domain.BaseObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

abstract class GenericManagerImplTest<T extends BaseObject> {

    protected T item;

    protected T item2;

    protected GenericDaoJpa<T> dao;

    protected GenericManagerImpl<T> manager;

    @Test
    void findOne() {
        String id = "12345";

        when(dao.findOne(id)).thenReturn(item);
        when(item.getId()).thenReturn(id);

        assertEquals(id, manager.findOne(id).getId());
    }

    @Test
    void findAll() {
        List<T> list = new ArrayList<>();
        list.add(item);
        list.add(item2);

        when(dao.findAll()).thenReturn(list);
        assertEquals(list, manager.findAll());
    }

    @Test
    void getCount() {
        when(dao.getCount()).thenReturn(2L);
        assertEquals(2L, manager.getCount());
    }

}