package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link Rule} entity.
 * <p>
 * Date: 23.11.17
 *
 * @author Marek Zimmermann
 */
public class RuleTest {
    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Create inside rule")
    void testCreateInsideRule() {
        Validity validity = getTestValidity();
        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.INSIDE);

        entityManager.persist(area);
        entityManager.persist(rule);

        Rule dbRule = entityManager.createQuery("select r from Rule r", Rule.class).getSingleResult();

        assertEquals(rule, dbRule);
    }

    @Test
    @DisplayName("Create outside rule")
    void testCreateOutsideRule() {
        Validity validity = getTestValidity();

        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 2",
                "another desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE);

        entityManager.persist(area);
        entityManager.persist(rule);

        Rule dbRule = entityManager.createQuery("select r from Rule r", Rule.class).getSingleResult();

        assertEquals(rule, dbRule);
    }

    @Test
    @DisplayName("Test null name")
    void testNullName() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            null,
                            "another desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test empty name")
    void testEmptyName() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "",
                            "another desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test blank name")
    void testBlankName() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "    ",
                            "another desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test null description")
    void testNullDescription() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            null,
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test null validity")
    void testNullValidity() {
        assertThrows(IllegalArgumentException.class,
                () -> new Rule(
                        "test rule 3",
                        "desc",
                        null,
                        Collections.singletonList(getTestArea()),
                        new ArrayList<>(),
                        new ArrayList<>(),
                        Rule.Type.OUTSIDE));
    }

    @Test
    @DisplayName("Test null areas")
    void testNullAreas() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            "desc",
                            validity,
                            null,
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test empty areas")
    void testEmptyAreas() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            "desc",
                            validity,
                            new ArrayList<>(),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test null vehicles")
    void testNullVehicles() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            "desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            null,
                            new ArrayList<>(),
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test null fleets")
    void testNullFleets() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            "desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            null,
                            Rule.Type.OUTSIDE);
                });
    }

    @Test
    @DisplayName("Test null type")
    void testNullType() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Validity validity = getTestValidity();

                    new Rule(
                            "test rule 3",
                            "desc",
                            validity,
                            Collections.singletonList(getTestArea()),
                            new ArrayList<>(),
                            new ArrayList<>(),
                            null);
                });
    }

    @Test
    @DisplayName("Test isApplicableAt with correct date, no time intervals")
    void testIsApplicableAtNoTsGoodDate() {
        Validity validity = getTestValidity();

        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 2",
                "another desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE);

        rule.getValidity().setMondayIntervals(new ArrayList<>());

        //random date within validity date bounds
        LocalDateTime testLdt = LocalDateTime.of(2017, 2, 1, 23, 15, 0);

        assertTrue(rule.isApplicableAt(testLdt));
    }

    @Test
    @DisplayName("Test isApplicableAt with out of bounds date, no time intervals")
    void testIsApplicableAtNoTsBadDate() {
        Validity validity = getTestValidity();

        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 2",
                "another desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE);

        rule.getValidity().setMondayIntervals(new ArrayList<>());

        //random date outside validity bounds
        LocalDateTime testLdt = LocalDateTime.of(2025, 2, 1, 23, 15, 0);

        assertFalse(rule.isApplicableAt(testLdt));
    }

    @Test
    @DisplayName("Test isApplicableAt with good date, within time interval")
    void testIsApplicableAtWithTsGoodDate() {
        Validity validity = getTestValidity();

        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 2",
                "another desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE);

        //monday date within validity date bounds
        LocalDateTime testLdt = LocalDateTime.of(2017, 12, 18, 8, 10, 0);

        assertTrue(rule.isApplicableAt(testLdt));
    }

    @Test
    @DisplayName("Test isApplicableAt with good date, outside time interval")
    void testIsApplicableAtWithTsGoodDateBadTime() {
        Validity validity = getTestValidity();

        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 2",
                "another desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<>(),
                new ArrayList<>(),
                Rule.Type.OUTSIDE);

        //monday date outside validity date bounds
        LocalDateTime testLdt = LocalDateTime.of(2017, 12, 18, 12, 10, 0);

        assertFalse(rule.isApplicableAt(testLdt));
    }

    private Area getTestArea() {
        return new Area
                (
                        "area",
                        Area.AreaType.RECTANGLE,
                        Arrays.asList
                                (
                                        new Position(1, 2, 0),
                                        new Position(2, 3, 1)
                                )
                );
    }

    private Validity getTestValidity() {
        return new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>()
        );
    }
}
