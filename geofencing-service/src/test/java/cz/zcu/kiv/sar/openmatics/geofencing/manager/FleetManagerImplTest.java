package cz.zcu.kiv.sar.openmatics.geofencing.manager;

import cz.zcu.kiv.sar.openmatics.geofencing.dao.FleetDao;
import cz.zcu.kiv.sar.openmatics.geofencing.dao.FleetDaoRest;
import cz.zcu.kiv.sar.openmatics.geofencing.extension.MockitoExtension;
import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FleetManagerImplTest {

    private Fleet item;

    private Fleet item2;

    private FleetManager fleetManager;

    private FleetDao fleetDao;

    @BeforeEach
    void setUp(@Mock FleetDaoRest fleetDao, @Mock Fleet item, @Mock Fleet item2) {
        this.item = item;
        this.item2 = item2;

        this.fleetDao = fleetDao;
        this.fleetManager = new FleetManagerImpl(fleetDao);
    }

    @Test
    void findAll() {
        List<Fleet> list = new ArrayList<>();
        list.add(item);
        list.add(item2);

        when(fleetDao.findAll()).thenReturn(list);
        assertEquals(list, fleetManager.findAll());
    }
}
