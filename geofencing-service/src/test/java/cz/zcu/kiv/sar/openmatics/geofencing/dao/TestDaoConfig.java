package cz.zcu.kiv.sar.openmatics.geofencing.dao;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;

@Configuration
// TODO Find JPA repositories automatically - https://stackoverflow.com/questions/22301426/no-qualifying-bean-of-type-javax-persistence-entitymanager
//@EnableJpaRepositories(basePackages = "cz.zcu.kiv.sar.openmatics.geofencing.dao")
@EnableTransactionManagement
public class TestDaoConfig {

    @Bean
    AreaDaoJpa areaDaoJpa() {
        return new AreaDaoJpa();
    }

    @Bean
    EventDaoJpa eventDaoJpa() {
        return new EventDaoJpa();
    }

    @Bean
    RuleDaoJpa ruleDaoJpa() {
        return new RuleDaoJpa();
    }

    @Bean
    RuleViolationDaoJpa ruleViolationDaoJpa() {
        return new RuleViolationDaoJpa();
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        return Persistence.createEntityManagerFactory("templatePU");
    }

    @Bean
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }
}
