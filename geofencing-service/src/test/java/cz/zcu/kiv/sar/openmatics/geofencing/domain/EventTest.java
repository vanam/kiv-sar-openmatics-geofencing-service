package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link Event} entity.
 * <p>
 * Date: 23.11.17
 *
 * @author Marek Zimmermann
 */
public class EventTest {
    private EntityManagerFactory entityManagerFactory;

    private EntityManager entityManager;

    @BeforeEach
    void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("templatePU");
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    @DisplayName("Create violation event - entering null area")
    void testCreateViolationInsideEvent() {
        Validity validity = getTestValidity();
        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.INSIDE);

        String vehicleId = "vid";

        LocalDateTime eventTime = LocalDateTime.of(2010, 10, 1, 1, 1, 0);

        Event event = new Event(
                Event.EventType.VIOLATION,
                rule,
                vehicleId,
                eventTime
        );

        entityManager.persist(area);
        entityManager.persist(validity);
        entityManager.persist(rule);
        entityManager.persist(event);

        Event dbEvent = entityManager.createQuery("select e from Event e", Event.class).getSingleResult();

        assertEquals(event, dbEvent);
    }

    @Test
    @DisplayName("Create correction event - exiting place outside of the area")
    void testCreateCorrectionOutsideEvent() {
        Validity validity = getTestValidity();
        Area area = getTestArea();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(area),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.OUTSIDE);

        String vehicleId = "vid";

        LocalDateTime eventTime = LocalDateTime.of(2010, 10, 1, 1, 1, 0);

        Event event = new Event(
                Event.EventType.CORRECTION,
                rule,
                vehicleId,
                eventTime
        );

        entityManager.persist(area);
        entityManager.persist(validity);
        entityManager.persist(rule);
        entityManager.persist(event);

        Event dbEvent = entityManager.createQuery("select e from Event e", Event.class).getSingleResult();

        assertEquals(event, dbEvent);
    }

    @Test
    @DisplayName("Test null type")
    void testNullType() {
        Validity validity = getTestValidity();

        Rule rule = new Rule(
                "test rule 1",
                "desc",
                validity,
                Collections.singletonList(getTestArea()),
                new ArrayList<VehicleId>(),
                new ArrayList<FleetId>(),
                Rule.Type.OUTSIDE);

        String vehicleId = "vid";

        LocalDateTime eventTime = LocalDateTime.of(2010, 10, 1, 1, 1, 0);

        assertThrows(IllegalArgumentException.class,
                () -> new Event(
                        null,
                        rule,
                        vehicleId,
                        eventTime
                )
        );
    }

    @Test
    @DisplayName("Test null rule")
    void testNullRule() {

        Area area = getTestArea();
        String vehicleId = "vid";

        LocalDateTime eventTime = LocalDateTime.of(2010, 10, 1, 1, 1, 0);

        assertThrows(IllegalArgumentException.class,
                () -> new Event(
                        Event.EventType.VIOLATION,
                        null,
                        vehicleId,
                        eventTime
                )
        );
    }

    // TODO test vehicle ID null

    // TODO test event time null

    private Validity getTestValidity() {
        return new Validity(
                new ValidationInterval(
                        LocalDate.of(2001, 1, 1),
                        LocalDate.of(2020, 12, 31)
                ),
                Collections.singletonList(new TimeInterval(LocalTime.of(1, 15), LocalTime.of(10, 30))),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>(),
                new ArrayList<TimeInterval>()
        );
    }

    private Area getTestArea() {
        return new Area(
                "name",
                Area.AreaType.RECTANGLE,
                Arrays.asList(
                        new Position(0, 0, 0),
                        new Position(0, 0, 1)));
    }
}
