package cz.zcu.kiv.sar.openmatics.geofencing.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TimeIntervalTest {

    @Test
    @DisplayName("Create validation interval")
    void testCreateValidationInterval() {
        TimeInterval interval = new TimeInterval(
                LocalTime.of(1, 15),
                LocalTime.of(10, 30)
        );

        assertEquals(LocalTime.of(1, 15), interval.getTimeFrom());
        assertEquals(LocalTime.of(10, 30), interval.getTimeTo());
    }

    @Test
    @DisplayName("Create validation interval with null time from")
    void testNullTimeFrom() {
        assertThrows(IllegalArgumentException.class,
                () -> new TimeInterval(
                        null,
                        LocalTime.of(10, 30)
                )
        );
    }

    @Test
    @DisplayName("Create validation interval with null time to")
    void testNullTimeTo() {
        assertThrows(IllegalArgumentException.class,
                () -> new TimeInterval(
                        LocalTime.of(1, 15),
                        null
                )
        );
    }
}