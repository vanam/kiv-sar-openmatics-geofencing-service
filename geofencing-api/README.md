# KIV/SAR - OpenMatics - Geofencing API

## Implementované endpointy

Ověřit fungování (GET) endpointu včetně HTTP kódu a *Content-Type* můžete pomocí `curl -i http://127.0.0.1:8080/{endpoint}`

### Seznam endpointů

* `/areas` - Seznam oblastí definovaných klientem
* `/events` - Seznam událostí (porušení/napravení pravidla) vozidel klienta
* `/fleets` - Seznam flotil daného klienta
* `/rules` - Seznam pravidel definovaných klientem pro dané vozidla/flotily
* `/vehicles` - Seznam vozidel daného klienta

## Tým

* [Petra Volenová](volenova@civ.zcu.cz)
* [Marek Zimmermann](zimmma@civ.zcu.cz)
* [Martin Váňa](vanam@students.zcu.cz)

## Zdroje

* [Jersey](https://jersey.github.io/)
* [Developing JAX-RS Services using Spring Boot](http://www.allprogrammingtutorials.com/tutorials/developing-jaxrs-services-using-spring-boot.php)