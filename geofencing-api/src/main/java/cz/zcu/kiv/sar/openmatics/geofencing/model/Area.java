package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Area object contains basic information defining the shape of the area where the rule is applied.
 */
public class Area {

    public enum AreaType {

        POLYGON("polygon"),
        RECTANGLE("rectangle"),
        CIRCLE("circle");

        private String shape;

        AreaType(String shape) {
            this.shape = shape;
        }

        public String getShape() {
            return shape;
        }
    }

    /** Area ID */
    private String id;

    /** Area name */
    private String name;

    /** Type of area shape */
    private AreaType type;

    /** List of positions which define this area */
    private List<Position> positionList;

    /** When was area created - time from epoch in milliseconds */
    private long createDateMillis;

    public Area() {
    }

    public Area(String id, String name, AreaType type, List<Position> positionList, long createDateMillis) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.positionList = positionList;
        this.createDateMillis = createDateMillis;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AreaType getType() {
        return type;
    }

    public void setType(AreaType type) {
        this.type = type;
    }

    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }

    public long getCreateDateMillis()
    {
        return createDateMillis;
    }

    public void setCreateDateMillis(long createDateMillis)
    {
        this.createDateMillis = createDateMillis;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("type", type)
                .append("positionList", positionList)
                .append("createDateMillis", createDateMillis)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        return new EqualsBuilder()
                .append(createDateMillis, area.createDateMillis)
                .append(id, area.id)
                .append(name, area.name)
                .append(type, area.type)
                .append(positionList, area.positionList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(type)
                .append(positionList)
                .append(createDateMillis)
                .toHashCode();
    }
}
