package cz.zcu.kiv.sar.openmatics.geofencing.model;


import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Rule object contains information for rule evaluation.
 */
public class Rule {

    public enum Type {

        INSIDE("inside"),
        OUTSIDE("outside");

        private String type;

        Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    /** Rule ID */
    private String id;
    /** Rule name */
    private String name;
    /** Rule information */
    private String description;
    /** Rule validity */
    private Validity validity;
    /** List of areas on which is rule applied */
    private List<Area> areaList;
    /** List of vehicles ID on which is rule applied */
    private List<String> vehicleIdList;
    /** List of fleets ID on which is rule applied */
    private List<String> fleetIdList;
    /** Type of rule which is evaluated */
    private Type type;

    public Rule() {
    }

    public Rule(String id, String name, String description, Validity validity, List<Area> areaList, List<String> vehicleIdList, List<String> fleetIdList, Type type) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.validity = validity;
        this.areaList = areaList;
        this.vehicleIdList = vehicleIdList;
        this.fleetIdList = fleetIdList;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Validity getValidity() {
        return validity;
    }

    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public List<String> getVehicleIdList() {
        return vehicleIdList;
    }

    public void setVehicleIdList(List<String> vehicleIdList) {
        this.vehicleIdList = vehicleIdList;
    }

    public List<String> getFleetIdList() {
        return fleetIdList;
    }

    public void setFleetIdList(List<String> fleetIdList) {
        this.fleetIdList = fleetIdList;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("validity", validity)
                .append("areaList", areaList)
                .append("vehicleIdList", vehicleIdList)
                .append("fleetIdList", fleetIdList)
                .append("type", type)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        return new EqualsBuilder()
                .append(id, rule.id)
                .append(name, rule.name)
                .append(description, rule.description)
                .append(validity, rule.validity)
                .append(areaList, rule.areaList)
                .append(vehicleIdList, rule.vehicleIdList)
                .append(fleetIdList, rule.fleetIdList)
                .append(type, rule.type)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(description)
                .append(validity)
                .append(areaList)
                .append(vehicleIdList)
                .append(fleetIdList)
                .append(type)
                .toHashCode();
    }
}
