package cz.zcu.kiv.sar.openmatics.geofencing.api;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Area;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/areas")
public interface IAreaService {

    /**
     * Returns area list related to a particular client.
     *
     * @return list of areas
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Area> getAreas();

    /**
     * Create Area in IoT Hub and return matching instance.
     *
     * @param area Area data
     * @return new Area instance
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Area addArea(Area area);

    /**
     * Update Area in IoT Hub and return matching instance.
     *
     * @param area Area data
     * @return updated Area instance
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Area updateArea(Area area);

    /**
     * Delete existing Area.
     *
     * @param id of Area to delete
     */
    @DELETE
    @Path("/{id}")
    void deleteArea(@PathParam("id") String id);

}
