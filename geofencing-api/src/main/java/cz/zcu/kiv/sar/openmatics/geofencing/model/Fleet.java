package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Fleet object contains group of vehicles for rule evaluation.
 */
public class Fleet {

    /** Fleet ID */
    private String id;
    /** Fleet name */
    private String name;
    /** List of vehicles in the fleet */
    private List<Vehicle> vehicles;

    public Fleet() {
    }

    public Fleet(String id, String name, List<Vehicle> vehicles) {
        this.id = id;
        this.name = name;
        this.vehicles = vehicles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("vehicles", vehicles)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Fleet fleet = (Fleet) o;

        return new EqualsBuilder()
                .append(id, fleet.id)
                .append(name, fleet.name)
                .append(vehicles, fleet.vehicles)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(vehicles)
                .toHashCode();
    }
}
