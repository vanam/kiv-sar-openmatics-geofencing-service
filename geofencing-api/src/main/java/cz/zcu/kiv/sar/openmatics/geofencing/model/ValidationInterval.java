package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

/**
 * Validation interval contains information for defining date range for rule evaluation.
 */
public class ValidationInterval {

    /** Start of validation interval in seconds */
    private long dateFrom;
    /** End of validation interval in seconds */
    private long dateTo;

    public ValidationInterval() {
    }

    public ValidationInterval(long dateFrom, long dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public long getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(long dateFrom) {
        this.dateFrom = dateFrom;
    }

    public long getDateTo() {
        return dateTo;
    }

    public void setDateTo(long dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("dateFrom", dateFrom)
                .append("dateTo", dateTo)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ValidationInterval that = (ValidationInterval) o;

        return new EqualsBuilder()
                .append(dateFrom, that.dateFrom)
                .append(dateTo, that.dateTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(dateFrom)
                .append(dateTo)
                .toHashCode();
    }
}