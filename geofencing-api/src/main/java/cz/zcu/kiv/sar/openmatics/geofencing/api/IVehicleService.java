package cz.zcu.kiv.sar.openmatics.geofencing.api;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Vehicle;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/vehicles")
public interface IVehicleService {

    /**
     * Returns vehicle list related to a particular client.
     *
     * @return list of vehicles
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Vehicle> getVehicles();

}
