package cz.zcu.kiv.sar.openmatics.geofencing.api;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Fleet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/fleets")
public interface IFleetService {

    /**
     * Returns fleet list related to a particular client.
     *
     * @return list of fleets
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Fleet> getFleets();

}
