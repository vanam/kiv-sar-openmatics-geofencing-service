package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Validity object contains time information for rule evaluation.
 */
public class Validity {

    /** Interval in which is validation verified */
    private ValidationInterval validationInterval;
    /** Array of days of week with time intervals */
    private List<TimeInterval>[] timeIntervals;

    public Validity() {
    }

    public Validity(ValidationInterval validationInterval, List<TimeInterval>[] timeIntervals) {
        this.validationInterval = validationInterval;
        this.timeIntervals = timeIntervals;
    }

    public ValidationInterval getValidationInterval() {
        return validationInterval;
    }

    public void setValidationInterval(ValidationInterval validationInterval) {
        this.validationInterval = validationInterval;
    }

    public List<TimeInterval>[] getTimeIntervals() {
        return timeIntervals;
    }

    public void setTimeIntervals(List<TimeInterval>[] timeIntervals) {
        this.timeIntervals = timeIntervals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("validationInterval", validationInterval)
                .append("timeIntervals", timeIntervals)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Validity validity = (Validity) o;

        return new EqualsBuilder()
                .append(validationInterval, validity.validationInterval)
                .append(timeIntervals, validity.timeIntervals)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(validationInterval)
                .append(timeIntervals)
                .toHashCode();
    }
}
