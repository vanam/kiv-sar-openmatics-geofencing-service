package cz.zcu.kiv.sar.openmatics.geofencing.api;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Rule;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/rules")
public interface IRuleService {

    /**
     * Returns rule list related to a particular client.
     *
     * @return list of rules
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Rule> getRules();

    /**
     * Create Rule in IoT Hub and return matching instance.
     *
     * @param ruleId Rule ID
     * @return duplicated Rule instance
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/rule/{ruleId}")
    Rule duplicateRule(@PathParam("ruleId") String ruleId);

    /**
     * Duplicate Rule in IoT Hub and return matching instance.
     *
     * @param rule Rule data
     * @return new Rule instance
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Rule addRule(Rule rule);

    /**
     * Update Rule in IoT Hub and return matching instance.
     *
     * @param rule Rule data
     * @return updated Rule instance
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Rule updateRule(Rule rule);

    /**
     * Delete existing Rule.
     *
     * @param id of Rule to delete
     */
    @DELETE
    @Path("/{id}")
    void deleteRule(@PathParam("id") String id);
}
