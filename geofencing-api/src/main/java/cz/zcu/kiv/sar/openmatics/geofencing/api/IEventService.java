package cz.zcu.kiv.sar.openmatics.geofencing.api;

import cz.zcu.kiv.sar.openmatics.geofencing.model.Event;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/events")
public interface IEventService {

    /**
     * Returns event list related to a particular client.
     *
     * @return list of events
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Event> getEvents();

}
