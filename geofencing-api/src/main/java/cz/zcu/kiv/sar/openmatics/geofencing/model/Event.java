package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Event object contains basic information about violation of the rule.
 */
public class Event {

    public enum EventType {

        VIOLATION("violation"),
        CORRECTION("correction");

        private String type;

        EventType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    /** Event ID */
    private String id;
    /** Event name */
    private EventType name;
    /** Rule that was violated. */
    private Rule rule;
    /** ID of a vehicle that caused the event to happen. */
    private String vehicleId;
    /** Date and time of rule violation in seconds */
    private long eventTime;

    public Event() {
    }

    public Event(String id, EventType name, Rule rule, String vehicleId, long eventTime) {
        this.id = id;
        this.name = name;
        this.rule = rule;
        this.vehicleId = vehicleId;
        this.eventTime = eventTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EventType getName() {
        return name;
    }

    public void setName(EventType name) {
        this.name = name;
    }

    public Rule getRule()
    {
        return rule;
    }

    public void setRule(Rule rule)
    {
        this.rule = rule;
    }

    public String getVehicleId()
    {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId)
    {
        this.vehicleId = vehicleId;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("rule", rule)
                .append("vehicleId", vehicleId)
                .append("eventTime", eventTime)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return new EqualsBuilder()
                .append(eventTime, event.eventTime)
                .append(id, event.id)
                .append(name, event.name)
                .append(rule, event.rule)
                .append(vehicleId, event.vehicleId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(rule)
                .append(vehicleId)
                .append(eventTime)
                .toHashCode();
    }
}
