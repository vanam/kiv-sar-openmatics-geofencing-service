package cz.zcu.kiv.sar.openmatics.geofencing.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalTime;

/**
 * Time interval object contains information for defining time of rule evaluation.
 */
public class TimeInterval {

    /** Interval start in seconds */
    private long timeFrom;
    /** Interval end in seconds */
    private long timeTo;

    public TimeInterval() {
    }

    public TimeInterval(long timeFrom, long timeTo) {
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public long getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(long timeFrom) {
        this.timeFrom = timeFrom;
    }

    public long getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(long timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("timeFrom", timeFrom)
                .append("timeTo", timeTo)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TimeInterval that = (TimeInterval) o;

        return new EqualsBuilder()
                .append(timeFrom, that.timeFrom)
                .append(timeTo, that.timeTo)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(timeFrom)
                .append(timeTo)
                .toHashCode();
    }
}
